$(document).ready(function () {

    $('#efind').on('click', function () {
        if ($("#eid_type").val.toString() && $("#eid_number")) {
            
            $.ajaxSetup({headers:{'csrftoken':'{{csrf_token()}}'}});
            
            $.post("/dashboard/item/areas/",{eid_type:$("#eid_type").val,eid_number:$("#eid_number")},function (data, status) {
                var response = $.parseJSON(data);
                var areas = response.areas;
                $("#area").empty();
                $.each(areas, function (index, element) {
                    $("#area").append($('<option>', {value: element.id, text: element.name}));

                });
            });

        } else {
            $("#eowner_details").hide();
            $("#ownerid").val("");
            $("#eowner_error_text").text("Enter ID Number and ID type");
            $("#eowner_error").show();
        }
    });

    $('#ereset').on('click', function () {
        $("#eowner_details").hide();
        $("#ownerid").val("");
        $("#eowner_error_text").text("Enter ID Number and ID type");
        $("#eowner_error").show();
    });

});


