<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blpermit extends Model
{
    /*
     * Get permit owner
     * @returns App\Owner
     */
    public function owner(){
        return $this->belongsTo("App\Owner","ownerid","ownerid");
    }
    
    
    /*
     * Get related property rate record
     * @returns App\Prate
     */
    public function prate(){
        return $this->belongsTo("App\Prate","prid","prid");
    }
    
    
    /*
     * Get related summons
     * @returns App\Summon
     */
    public function summon(){
        return $this->hasOne("App\Summon","pid","blpid");
    }
    
    /*
     * Get receipts
     * @returns App\Receipt
     */
    public function receipts(){
        return $this->hasMany("App\Receipt","pid","blpid");
    }
    
    
}
