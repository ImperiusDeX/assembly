<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    /*
     * Get related property rate
     * @returns App\Prate
     */
    public function prate(){
        $this->belongsTo("App\Prate","prid","pid");
    }
    
    /*
     * Get related business permit
     * @returns App\Bupermit
     */
    public function bupermit(){
        $this->belongsTo("App\Bupermit","bupid","pid");
    }
    
    /*
     * Get related building permit
     * @returns App\Blpermit
     */
    public function blpermit(){
        $this->belongsTo("App\Blpermit","blpid","pid");
    }
    
    /*
     * Get related Temp Structure permit
     * @returns App\Tspermit
     */
    public function tspermit(){
        $this->belongsTo("App\Tspermit","tspid","pid");
    }
    
    /*
     * Get related Signage & Advert permit
     * @returns App\Spermit
     */
    public function spermit(){
        $this->belongsTo("App\Spermit","spid","pid");
    }
}
