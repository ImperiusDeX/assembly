<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**public function roles() {
        return $this->hasMany("App\Role", "userid", "id");
    }**/

    public function hasRole($section, $permission) {
        /**
         * Sections 1 Prates 2 Blpermit 3 Bupermit 4 Spermit 5 Tspermit 6 Summon 0 Admin
         * Permissions 1 Due 2 Add 3 View 0 Admin
         * 
         * {section:[permissions],..}
         */
        $role = $this->where(function($query) use ($section, $permission) {
                    $query->whereJsonContains("roles->$section", $permission);
                    $query->orWhereJsonContains("roles->0", 0);
                })->first();
        if (!empty($role)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function hasAnyRole($permission) {
        $role = $this->where(function($query) use ($permission) {
                    $query->whereJsonContains("roles->1", $permission);
                    $query->orWhereJsonContains("roles->2", $permission);
                    $query->orWhereJsonContains("roles->3", $permission);
                    $query->orWhereJsonContains("roles->4", $permission);
                    $query->orWhereJsonContains("roles->5", $permission);
                    $query->orWhereJsonContains("roles->6", $permission);
                    $query->orWhereJsonContains("roles->0", 0);
                })->first();
        if (!empty($role)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
