<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spermit extends Model
{
    
    /*
     * Get related property rate record
     * @returns App\Prate
     */
    public function prate(){
        return $this->belongsTo("App\Prate","prid","prid");
    }
    
    /*
     * Get permit owner
     * @returns App\Owner
     */
    public function owner(){
        return $this->belongsTo("App\Owner","ownerid","ownerid");
    }
    
    /*
     * Get related summons
     * @returns App\Summon
     */
    public function summon(){
        return $this->hasOne("App\Summon","pid","spid");
    }
    
    
    /*
     * Get receipts
     * @returns App\Receipt
     */
    public function receipts(){
        return $this->hasMany("App\Receipt","pid","spid");
    }
}
