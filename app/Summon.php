<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Summon extends Model
{
    /*
     * Get related property rate record
     * @returns App\Prate
     */
    public function prate(){
        return $this->belongsTo("App\Prate","prid","prid");
    }
    
}
