<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    
    /**
     * Gets the owner's Property Rate record
     * @returns App\Prate
     */
    public function prates(){
        return $this->hasMany("App\Prate","ownerid","ownerid");
    }
    
    
    /**
     * Gets the owner's Building permit records
     * @returns App\Blpermit
     */
    public function blpermits(){
        return $this->hasMany("App\Blpermit","ownerid","ownerid");
    }
    
    
    /**
     * Gets the owner's Business Permit records
     * @returns App\Bupermit
     */
    public function bupermits(){
        return $this->hasMany("App\Bupermit","ownerid","ownerid");
    }
    
    
    /**
     * Gets the owner's Signage and Advert permit records
     * @returns App\Spermit
     */
    public function spermits(){
        return $this->hasMany("App\Spermit","ownerid","ownerid");
    }
    
    /**
     * Gets the owner's Temporary Structure permit records
     * @returns App\Tspermit
     */
    public function tspermits(){
        return $this->hasMany("App\Tspermit","ownerid","ownerid");
    }
    
   
    
}
