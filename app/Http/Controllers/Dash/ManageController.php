<?php

namespace App\Http\Controllers\Dash;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Illuminate\Support\Facades\Input;
use App\Http\Requests\AddUserRequest;
use App\User;
use App\Role;

class ManageController extends Controller {

    public function viewUsers() {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $terms = Input::get("query");


        $users = User::where("district", "=", $district)
                        ->whereRaw('json_contains(`roles`->\'$."0"\', \'0\') IS NULL')
                        ->where(function($query) use ($terms) {
                            foreach (explode(",", $terms) as $term) {
                                $query->where(function($squery) use ($term) {
                                    $squery->where("name", "LIKE", "%$term%")
                                    ->orWhere("email", "LIKE", "%$term%")
                                    ->orWhere("personal_email", "LIKE", "%$term%")
                                    ->orWhere("phone", "LIKE", "%$term%");
                                });
                            }
                        })->orderBy("users.id", "desc")->paginate(20);


        return view("viewusers", ["users" => $users, "query" => $terms]);
    }

    public function viewUserDetails($userid) {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;


        $user = User::where([["district", "=", $district],
                        ["users.id", "=", $userid]])
                ->whereRaw('json_contains(`roles`->\'$."0"\', \'0\') IS NULL')
                ->first();

        if (!empty($user)) {
            return view("viewuserdetails", ["user" => $user]);
        } else {
            abort(404);
        }
    }

    public function addUserForm() {
        return view("adduserform");
    }

    public function addUser(AddUserRequest $request) {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $nuser = new User();
        $nuser->name = $request->surname . " " . $request->first_name;
        $nuser->personal_email = $request->email;
        $nuser->phone = $request->phone;
        $nuser->role = json_encode(["1" => (!empty($request->prate)) ? array_map('intval', $request->prate) : [], "2" => (!empty($request->blpermit)) ? array_map('intval', $request->blpermit) : [], "3" => (!empty($request->bupermit)) ? array_map('intval', $request->bupermit) : [],
            "4" => (!empty($request->spermit)) ? array_map('intval', $request->spermit) : [], "5" => (!empty($request->tspermit)) ? array_map('intval', $request->tspermit) : [], "6" => (!empty($request->summon)) ? array_map('intval', $request->summon) : []]);
        $nuser->district = $district;
        $nuser->email = strtolower($request->surname . $request->first_name . "@assembly." . $district);
        $nuser->password = bcrypt($request->password);


        if ($nuser->save()) {
            return back()->with('server_success', 'User added');
        } else {
            return back()->with('server_error', 'An error was encountered. Please try again later');
        }
    }

    public function removeUser($userid) {
        $user = auth()->user();
        $district = $user->district;


        $user = User::where([["district", "=", $district],
                        ["users.id", "=", $userid]])
                ->whereRaw('json_contains(`roles``->\'$."0"\', \'0\') IS NULL')
                ->first();

        if ($user->delete()) {
            return redirect()->route("viewUsers")->with("server_success", "User successfully removed");
        } else {
            abort(404);
        }
    }

    public function genUserId() {
        return uniqid();
    }

}
