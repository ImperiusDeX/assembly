<?php

namespace App\Http\Controllers\Dash;

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\User;
use App\Prate;
use App\Blpermit;
use App\Bupermit;
use App\Spermit;
use App\Tspermit;

class DueController extends Controller {

    /**
     * Show due property rates
     *
     * @return View
     */
    public function duePrates() {
        try{
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $terms = Input::get("query");
        $from = Input::get("from");
        $to = Input::get("to");

        if (empty($from) || empty($to)) {
            $year = date("Y");
            $from = "$year-01-01";
            $to = "$year-12-31";
        }

        if (empty($terms)) {
            $terms = "";
        }

        $due = Prate::where([["prev_paid", "=", "0"],
                        ["district", "=", $district]])
                ->whereDate("prates.created_at", ">=", $from)
                ->whereDate("prates.created_at", "<=", $to)
                ->join("owners", "prates.ownerid", "=", "owners.ownerid")
                ->select("owners.name", "prates.prid", "prates.total", "prates.paid", "prates.created_at")
                ->where(function($query) use ($terms) {
                    foreach (explode(",", $terms) as $term) {
                        $query->where(function($squery) use ($term) {
                            $squery->where("name", "LIKE", "%$term%")
                            ->orWhere("phone", "LIKE", "%$term%")
                            ->orWhere("email", "LIKE", "%$term%")
                            ->orWhere("id_number", "LIKE", "%$term%")
                            ->orWhere("location", "LIKE", "%$term%");
                        });
                    }
                })->orderBy("prates.created_at", "desc")
                ->paginate(20);

        return view('dueprate', ["prates" => $due, "query" => $terms, "from" => $from, "to" => $to]);
        }catch(Exception $e){
            
        }
    }


    /**
     * Show due building permits
     *
     * @return View
     */
    public function dueBlpermits() {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $terms = Input::get("query");
        $from = Input::get("from");
        $to = Input::get("to");

        if (empty($from) || empty($to)) {
            $year = date("Y");
            $from = "$year-01-01";
            $to = "$year-12-31";
        }

        if (empty($term)) {
            $term = "";
        }

        $due = Blpermit::where([["blpermits.prev_paid", "=", "0"],
                        ["prates.district", "=", $district]])
                ->whereDate("blpermits.created_at", ">=", $from)
                ->whereDate("blpermits.created_at", "<=", $to)
                ->select("blpermits.total", "blpermits.paid", "owners.name", "blpermits.blpid", "blpermits.created_at")
                ->where(function($query) use ($terms) {
                    foreach (explode(",", $terms) as $term) {
                        $query->where(function($squery) use ($term) {
                            $squery->where("name", "LIKE", "%$term%")
                            ->orWhere("phone", "LIKE", "%$term%")
                            ->orWhere("email", "LIKE", "%$term%")
                            ->orWhere("id_number", "LIKE", "%$term%")
                            ->orWhere("location", "LIKE", "%$term%");
                        });
                    }
                })
                ->join("owners", "blpermits.ownerid", "=", "owners.ownerid")
                ->join("prates", "blpermits.prid", "=", "prates.prid")
                ->orderBy("blpermits.created_at", "desc")
                ->paginate(20);
        return view('dueblpermit', ["blpermits" => $due, "query" => $terms, "from" => $from, "to" => $to]);
    }


    /**
     * Show due business permits
     *
     * @return View
     */
    public function dueBupermits() {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $terms = Input::get("query");
        $from = Input::get("from");
        $to = Input::get("to");

        if (empty($from) || empty($to)) {
            $year = date("Y");
            $from = "$year-01-01";
            $to = "$year-12-31";
        }

        if (empty($term)) {
            $term = "";
        }

        $due = Bupermit::where([["bupermits.prev_paid", "=", "0"],
                        ["prates.district", "=", $district]])
                ->whereDate("bupermits.created_at", ">=", $from)
                ->whereDate("bupermits.created_at", "<=", $to)
                ->select("bupermits.total", "bupermits.paid", "owners.name", "bupermits.bupid", "bupermits.created_at")
                ->where(function($query) use ($terms) {
                    foreach (explode(",", $terms) as $term) {
                        $query->where(function($squery) use ($term) {
                            $squery->where("name", "LIKE", "%$term%")
                            ->orWhere("phone", "LIKE", "%$term%")
                            ->orWhere("email", "LIKE", "%$term%")
                            ->orWhere("id_number", "LIKE", "%$term%")
                            ->orWhere("location", "LIKE", "%$term%");
                        });
                    }
                })
                ->join("owners", "bupermits.ownerid", "=", "owners.ownerid")
                ->join("prates", "bupermits.prid", "=", "prates.prid")
                ->orderBy("bupermits.created_at", "desc")
                ->paginate(20);
        return view('duebupermit', ["bupermits" => $due, "query" => $terms, "from" => $from, "to" => $to]);
    }

    /**
     * Show due temporary structure permits
     *
     * @return View
     */
    public function dueTspermits() {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $terms = Input::get("query");
        $from = Input::get("from");
        $to = Input::get("to");

        if (empty($from) || empty($to)) {
            $year = date("Y");
            $from = "$year-01-01";
            $to = "$year-12-31";
        }

        if (empty($term)) {
            $term = "";
        }

        $due = Tspermit::where([["tspermits.prev_paid", "=", "0"],
                        ["prates.district", "=", $district]])
                ->whereDate("tspermits.created_at", ">=", $from)
                ->whereDate("tspermits.created_at", "<=", $to)
                ->select("tspermits.total", "tspermits.paid", "owners.name", "tspermits.tspid", "tspermits.created_at")
                ->where(function($query) use ($terms) {
                    foreach (explode(",", $terms) as $term) {
                        $query->where(function($squery) use ($term) {
                            $squery->where("name", "LIKE", "%$term%")
                            ->orWhere("phone", "LIKE", "%$term%")
                            ->orWhere("email", "LIKE", "%$term%")
                            ->orWhere("id_number", "LIKE", "%$term%")
                            ->orWhere("location", "LIKE", "%$term%");
                        });
                    }
                })
                ->join("owners", "tspermits.ownerid", "=", "owners.ownerid")
                ->join("prates", "tspermits.prid", "=", "prates.prid")
                ->orderBy("tspermits.created_at", "desc")
                ->paginate(20);
        return view('duetspermit', ["tspermits" => $due, "query" => $terms, "from" => $from, "to" => $to]);
    }

    /**
     * Show due signage and advert permits
     *
     * @return View
     */
    public function dueSpermits() {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $terms = Input::get("query");
        $from = Input::get("from");
        $to = Input::get("to");

        if (empty($from) || empty($to)) {
            $year = date("Y");
            $from = "$year-01-01";
            $to = "$year-12-31";
        }

        if (empty($term)) {
            $term = "";
        }

        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $due = Spermit::where([["spermits.prev_paid", "=", "0"],
                        ["prates.district", "=", $district]])
                ->whereDate("spermits.created_at", ">=", $from)
                ->whereDate("spermits.created_at", "<=", $to)
                ->select("spermits.total", "spermits.paid", "owners.name", "spermits.spid", "spermits.created_at")
                ->where(function($query) use ($terms) {
                    foreach (explode(",", $terms) as $term) {
                        $query->where(function($squery) use ($term) {
                            $squery->where("name", "LIKE", "%$term%")
                            ->orWhere("phone", "LIKE", "%$term%")
                            ->orWhere("email", "LIKE", "%$term%")
                            ->orWhere("id_number", "LIKE", "%$term%")
                            ->orWhere("location", "LIKE", "%$term%");
                        });
                    }
                })
                ->join("owners", "spermits.ownerid", "=", "owners.ownerid")
                ->join("prates", "spermits.prid", "=", "prates.prid")
                ->orderBy("spermits.created_at", "desc")
                ->paginate(20);
        return view('duespermit', ["spermits" => $due, "query" => $terms, "from" => $from, "to" => $to]);
    }

}
