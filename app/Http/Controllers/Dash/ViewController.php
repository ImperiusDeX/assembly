<?php

namespace App\Http\Controllers\Dash;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Prate;
use App\Blpermit;
use App\Bupermit;
use App\Spermit;
use App\Tspermit;
use App\Summon;
use App\Owner;
use App\Receipt;

class ViewController extends Controller
{
    
    /**
     * Show all property rate records
     * 
     * @return View
     */
    public function viewPrates(){
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $terms = Input::get("query");
        $from = Input::get("from");
        $to = Input::get("to");

        if (empty($from) || empty($to)) {
            $year = date("Y");
            $from = "$year-01-01";
            $to = "$year-12-31";
        }

        if (empty($terms)) {
            $terms = "";
        }

        $views = Prate::where("district", "=", $district)
                ->whereDate("prates.created_at", ">=", $from)
                ->whereDate("prates.created_at", "<=", $to)
                ->join("owners", "prates.ownerid", "=", "owners.ownerid")
                ->select("owners.name", "prates.prid", "prates.total", "prates.paid", "prates.created_at")
                ->where(function($query) use ($terms) {
                    foreach (explode(",", $terms) as $term) {
                        $query->where(function($squery) use ($term) {
                            $squery->where("name", "LIKE", "%$term%")
                            ->orWhere("phone", "LIKE", "%$term%")
                            ->orWhere("email", "LIKE", "%$term%")
                            ->orWhere("id_number", "LIKE", "%$term%")
                            ->orWhere("location", "LIKE", "%$term%");
                        });
                    }
                })->orderBy("prates.created_at", "desc")
                ->paginate(20);

        return view('viewprates', ["prates" => $views, "query" => $terms, "from" => $from, "to" => $to]);
    }
    
    /**
     * Show all building permit records
     * 
     * @return View
     */
    public function viewBlpermits(){
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $terms = Input::get("query");
        $from = Input::get("from");
        $to = Input::get("to");

        if (empty($from) || empty($to)) {
            $year = date("Y");
            $from = "$year-01-01";
            $to = "$year-12-31";
        }

        if (empty($term)) {
            $term = "";
        }

        $views = Blpermit::where("prates.district", "=", $district)
                ->whereDate("blpermits.created_at", ">=", $from)
                ->whereDate("blpermits.created_at", "<=", $to)
                ->select("blpermits.total", "blpermits.paid", "owners.name", "blpermits.blpid", "blpermits.created_at")
                ->where(function($query) use ($terms) {
                    foreach (explode(",", $terms) as $term) {
                        $query->where(function($squery) use ($term) {
                            $squery->where("name", "LIKE", "%$term%")
                            ->orWhere("phone", "LIKE", "%$term%")
                            ->orWhere("email", "LIKE", "%$term%")
                            ->orWhere("id_number", "LIKE", "%$term%")
                            ->orWhere("location", "LIKE", "%$term%");
                        });
                    }
                })
                ->join("owners", "blpermits.ownerid", "=", "owners.ownerid")
                ->join("prates", "blpermits.prid", "=", "prates.prid")
                ->orderBy("blpermits.created_at", "desc")
                ->paginate(3);
        return view('viewblpermits', ["blpermits" => $views, "query" => $terms, "from" => $from, "to" => $to]);
    }
    
    /**
     * Show all business permit records
     * 
     * @return View
     */
    public function viewBupermits(){
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $terms = Input::get("query");
        $from = Input::get("from");
        $to = Input::get("to");

        if (empty($from) || empty($to)) {
            $year = date("Y");
            $from = "$year-01-01";
            $to = "$year-12-31";
        }

        if (empty($term)) {
            $term = "";
        }

        $views = Bupermit::where("prates.district", "=", $district)
                ->whereDate("bupermits.created_at", ">=", $from)
                ->whereDate("bupermits.created_at", "<=", $to)
                ->select("bupermits.total", "bupermits.paid", "owners.name", "bupermits.bupid", "bupermits.created_at")
                ->where(function($query) use ($terms) {
                    foreach (explode(",", $terms) as $term) {
                        $query->where(function($squery) use ($term) {
                            $squery->where("name", "LIKE", "%$term%")
                            ->orWhere("phone", "LIKE", "%$term%")
                            ->orWhere("email", "LIKE", "%$term%")
                            ->orWhere("id_number", "LIKE", "%$term%")
                            ->orWhere("location", "LIKE", "%$term%");
                        });
                    }
                })
                ->join("owners", "bupermits.ownerid", "=", "owners.ownerid")
                ->join("prates", "bupermits.prid", "=", "prates.prid")
                ->orderBy("bupermits.created_at", "desc")
                ->paginate(20);
        return view('viewbupermits', ["bupermits" => $views, "query" => $terms, "from" => $from, "to" => $to]);
    }
    
    
    
    /**
     * Show all signage permit records
     * 
     * @return View
     */
    public function viewSpermits(){
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $terms = Input::get("query");
        $from = Input::get("from");
        $to = Input::get("to");

        if (empty($from) || empty($to)) {
            $year = date("Y");
            $from = "$year-01-01";
            $to = "$year-12-31";
        }

        if (empty($term)) {
            $term = "";
        }

        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $views = Spermit::where("district", "=", $district)
                ->whereDate("spermits.created_at", ">=", $from)
                ->whereDate("spermits.created_at", "<=", $to)
                ->select("spermits.total", "spermits.paid", "owners.name", "spermits.spid", "spermits.created_at")
                ->where(function($query) use ($terms) {
                    foreach (explode(",", $terms) as $term) {
                        $query->where(function($squery) use ($term) {
                            $squery->where("name", "LIKE", "%$term%")
                            ->orWhere("phone", "LIKE", "%$term%")
                            ->orWhere("email", "LIKE", "%$term%")
                            ->orWhere("id_number", "LIKE", "%$term%")
                            ->orWhere("location", "LIKE", "%$term%");
                        });
                    }
                })
                ->join("owners", "spermits.ownerid", "=", "owners.ownerid")
                ->join("prates", "spermits.prid", "=", "prates.prid")
                ->orderBy("spermits.created_at", "desc")
                ->paginate(20);
        return view('viewspermits', ["spermits" => $views, "query" => $terms, "from" => $from, "to" => $to]);
    }
    
    /**
     * Show all temporary structure permit records
     * 
     * @return View
     */
    public function viewTspermits(){
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $terms = Input::get("query");
        $from = Input::get("from");
        $to = Input::get("to");

        if (empty($from) || empty($to)) {
            $year = date("Y");
            $from = "$year-01-01";
            $to = "$year-12-31";
        }

        if (empty($term)) {
            $term = "";
        }

        $views = Tspermit::where("district", "=", $district)
                ->whereDate("tspermits.created_at", ">=", $from)
                ->whereDate("tspermits.created_at", "<=", $to)
                ->select("tspermits.total", "tspermits.paid", "owners.name", "tspermits.tspid", "tspermits.created_at")
                ->where(function($query) use ($terms) {
                    foreach (explode(",", $terms) as $term) {
                        $query->where(function($squery) use ($term) {
                            $squery->where("name", "LIKE", "%$term%")
                            ->orWhere("phone", "LIKE", "%$term%")
                            ->orWhere("email", "LIKE", "%$term%")
                            ->orWhere("id_number", "LIKE", "%$term%")
                            ->orWhere("location", "LIKE", "%$term%");
                        });
                    }
                })
                ->join("owners", "tspermits.ownerid", "=", "owners.ownerid")
                ->join("prates", "tspermits.prid", "=", "prates.prid")
                ->orderBy("tspermits.created_at", "desc")
                ->paginate(20);
        return view('viewtspermits', ["tspermits" => $views, "query" => $terms, "from" => $from, "to" => $to]);
    }
    
    /**
     * Show all summons records
     * 
     * @return View
     */
    public function viewSummons(){
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $terms = Input::get("query");
        $from = Input::get("from");
        $to = Input::get("to");

        if (empty($from) || empty($to)) {
            $year = date("Y");
            $from = "$year-01-01";
            $to = "$year-12-31";
        }

        if (empty($term)) {
            $term = "";
        }

        $views = Summon::where("summons.district", "=", $district)
                ->whereDate("summons.created_at", ">=", $from)
                ->whereDate("summons.created_at", "<=", $to)
            
                ->where(function($query) use ($terms) {
                    foreach (explode(",", $terms) as $term) {
                        $query->where(function($squery) use ($term) {
                            $squery->where("name", "LIKE", "%$term%")
                            ->orWhere("phone", "LIKE", "%$term%")
                            ->orWhere("stage_of_const", "LIKE", "%$term%")
                            ->orWhere("type_of_building", "LIKE", "%$term%")
                            ->orWhere("location", "LIKE", "%$term%");
                        });
                    }
                })
                ->join("prates", "summons.prid", "=", "prates.prid")
                ->join("owners", "prates.ownerid", "=", "owners.ownerid")        
                ->orderBy("summons.created_at", "desc")
                ->paginate(20);
        return view('viewsummons', ["summons" => $views, "query" => $terms, "from" => $from, "to" => $to]);
    }
    
    
    /**
     * Show property rate record details
     * @param prid property rate id
     * @return View
     */
    public function viewPrateDetails($prid){
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $prate = Prate::where([["district", "=", $district],
                        ["prid", "=", $prid]])->first();
        if (!empty($prate)) {
            $owner = $prate->owner()->first();
            $receipts = $prate->receipts()->get();
            return view("viewpratedetails", ["prate" => $prate, "owner" => $owner,"receipts"=>$receipts]);
        } else {
            abort(404);
        }
    }
    
    /**
     * Show building permit record details
     * @param blpid building permit id
     * @return View
     */
    public function viewBlpermitDetails($blpid){
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $blpermit = Blpermit::where([
                        ["prates.district", "=", $district],
                        ["blpid", "=", $blpid]])
                        ->select("blpermits.type_of_building","blpermits.total","blpermits.paid","blpermits.blpid","blpermits.prid","blpermits.ownerid")
                 ->join("prates", "blpermits.prid", "=", "prates.prid")
                ->first();
        if (!empty($blpermit)) {
            $summon = $blpermit->summon()->first();
            $owner = $blpermit->owner()->first();
            $prate=$blpermit->prate()->first();
            $receipts = $blpermit->receipts()->get();
            $result = ["blpermit" => $blpermit, "owner" => $owner,"prate"=>$prate,"receipts"=>$receipts,"summon"=>$summon];
            return view("viewblpermitdetails", $result);
        } else {
            abort(404);
        }
    }
    
    /**
     * Show business permit record details
     * @param bupid business permit id
     * @return View
     */
    public function viewBupermitDetails($bupid){
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $bupermit = Bupermit::where([["prates.district", "=", $district],
                        ["bupid", "=", $bupid]])
                        ->select("bupermits.name_of_business","bupermits.total","bupermits.paid","bupermits.bupid","bupermits.prid","bupermits.ownerid")
                 ->join("prates", "bupermits.prid", "=", "prates.prid")
                ->first();
        if (!empty($bupermit)) {
            $owner = $bupermit->owner()->first();
            $prate=$bupermit->prate()->first();
            $receipts = $bupermit->receipts()->get();
            $result = ["bupermit" => $bupermit, "owner" => $owner,"prate"=>$prate,"receipts"=>$receipts];
            return view("viewbupermitdetails", $result);
        } else {
            abort(404);
        }
    }
    
    /**
     * Show sigange permit record details
     * @param spid signage permit id
     * @return View
     */
    public function viewSpermitDetails($spid){
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $spermit = Spermit::where([["spermits.prev_paid", "=", "0"],
                                ["prates.district", "=", $district],
                                ["spid", "=", $spid]])
                        ->select("spermits.size_of_signage", "spermits.property_type", "spermits.total", "spermits.paid", "spermits.spid", "spermits.prid", "spermits.ownerid")
                        ->join("prates", "spermits.prid", "=", "prates.prid")->first();
        if (!empty($spermit)) {
            $summon = $spermit->summon()->first();
            $owner = $spermit->owner()->first();
            $receipts = $spermit->receipts()->get();
            $prate=$spermit->prate()->first();
            $result = ["spermit" => $spermit, "owner" => $owner,"receipts"=>$receipts,"summon"=>$summon,"prate"=>$prate];
            
            return view("viewspermitdetails", $result);
        } else {
            abort(404);
        }
    }
    
    /**
     * Show temporary structure permit record details
     * @param tspid temp structure id
     * @return View
     */
    public function viewTspermitDetails($tspid){
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $tspermit = Tspermit::where([["tspermits.prev_paid", "=", "0"],
                        ["prates.district", "=", $district],
                        ["tspid", "=", $tspid]])
                ->select("tspermits.type_of_building", "tspermits.property_type", "tspermits.total", "tspermits.paid", "tspermits.tspid", "tspermits.prid", "tspermits.ownerid")
                ->join("prates", "tspermits.prid", "=", "prates.prid")
                ->first();
        if (!empty($tspermit)) {
            $summon = $tspermit->summon()->first();
            $owner = $tspermit->owner()->first();
            $receipts = $tspermit->receipts()->get();
            $prate=$tspermit->prate()->first();
            $result = ["tspermit" => $tspermit, "owner" => $owner,"receipts"=>$receipts,"summon"=>$summon,"prate"=>$prate];
            
            return view("viewtspermitdetails", $result);
        } else {
            abort(404);
        }
    }
    
    /**
     * Show summon record details
     * @param suid summon id
     * @return View
     */
    public function viewSummonDetails($suid){
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $summon = Summon::where([["district", "=", $district],
                        ["suid", "=", $suid]])->first();
        if (!empty($summon)) {
            $prate=$summon->prate()->first();
            return view("viewsummondetails", ["summon" => $summon,"prate"=>$prate]);
        } else {
            abort(404);
        }
    }
    
    
    
    
}
