<?php

namespace App\Http\Controllers\Dash;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddPrateRequest;
use App\Http\Requests\AddBlpermitRequest;
use App\Http\Requests\AddSpermitRequest;
use App\Http\Requests\AddTspermitRequest;
use App\Http\Requests\AddBupermitRequest;
use App\Http\Requests\AddSummonRequest;
use Illuminate\Support\Facades\Storage;
use App\Owner;
use App\Prate;
use App\Blpermit;
use App\Bupermit;
use App\Tspermit;
use App\Spermit;
use App\User;
use App\Summon;
use App\Receipt;
use Exception;

class AddController extends Controller {

    /**
     * Add property rate
     * @param AddPrateRequest request
     * @return View
     */
    public function addPrate(AddPrateRequest $request) {
        try {
            $owner = $this->saveOwner($request);

            $prate_owner_id = $owner["ownerid"];
            $user = auth()->user();
            $district = $user->district;

            if ($prate_owner_id) {
                $prate_location = $request->location;
                $prate_size = $request->size;


                /* if ($request->paid > 0) {
                  $prate_paid = $request->paid;
                  } else {
                  $prate_paid = 0;
                  }

                  if($appfee){
                  if($request->paid>=$appfee){
                  $prate_paid=$appfee;
                  }else{
                  return back()->with('server_error', 'Incorrect amount');
                  }
                  }
                 * 
                 */

                $prate_fee = $request->fee;

                $prate_id = $this->genPrateId();

                $prate = new Prate();
                $prate->prid = $prate_id;
                $prate->ownerid = $prate_owner_id;
                $prate->location = $prate_location;
                $prate->ghpost_code = $request->ghpost;
                $prate->size = $prate_size;
                $prate->total = $prate_fee;
                //$prate->paid = $prate_paid;
                $prate->district = $district;

                //create property rate
                if ($prate->save()) {

                    /* if ($prate_paid > 0) {
                      $this->saveReceipt($request, $prate_id, $prate_paid);
                      } */

                    return view("addsuccess", ['prid' => $prate_id]);
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception $e) {
            //delete new owner
            if (isset($owner) && !$owner["owner_exists"]) {
                Owner::where("ownerid", "=", $owner["ownerid"])->first()->delete();
                Storage::delete('public/profiles/' . $owner["ownerid"] . 'jpg');
            }
            return back()->with('server_error', 'An error was encountered creating record. Please try again later');
        }
    }

    /**
     * Get existing owner details
     * @param Request request
     * @return JSON
     */
    public function getOwner(Request $request) {

        $id_type = $request->eid_type;
        $id_number = $request->eid_number;

        $owner = Owner::where([["id_type", "=", $id_type],
                        ["id_number", "=", $id_number]])->first();

        if (!empty($owner)) {
            return json_encode(["status" => 80, "id" => $owner->ownerid, "name" => $owner->name,
                "picture" => asset("storage/profiles/" . $owner->ownerid . ".jpg"), "email" => $owner->email, "occupation" => $owner->occupation, "phone" => $owner->phone, "remarks" => json_decode($owner->extra)->remarks]);
        } else {
            return json_encode(["status" => 90]);
        }
    }

    /**
     * Add Building permit
     * @param AddBlpermitRequest request
     * @return View
     */
    public function addBlpermit(AddBlpermitRequest $request) {

        try {
            
            $user = $request->user();
            $district = $user->district;
            $prid = $request->prid;

            $prate = Prate::where([["prid", "=", $prid], ["district", "=", $district]])->first();
            if (!empty($prate)) {
                $ownerid = $prate->ownerid;
                $penalty = 0;
                $fee = doubleval($request->fee);

                /*                 * if ($appfee) {
                  if ($request->paid == $appfee) {
                  $paid = $appfee;
                  } else {
                  return back()->with('server_error', 'Incorrect amount');
                  }
                  }* */

                if ($request->has("suid")) {
                    $suid = $request->suid;
                    $summon = Summon::where([["suid", "=", $suid],
                        ["prid","=",$prid]])->whereYear("created_at", date("Y"))->whereNull("pid")->first();
                    if (!empty($summon)) {
                        $penalty = doubleval($summon->penalty);
                    } else {
                        throw new Exception('Summon not found');
                    }
                }

                $blpid = $this->genBlpermitId();
                $blpermit = new Blpermit();
                $blpermit->prid = $prid;
                $blpermit->blpid = $blpid;
                $blpermit->ownerid = $ownerid;
                $blpermit->type_of_building = $request->type;
                $blpermit->fee = $fee;
                $blpermit->penalty = $penalty;
                $blpermit->total = $fee + $penalty;
                //$blpermit->paid = $paid;

                if ($blpermit->save()) {
                    if (isset($summon) && !empty($summon)) {
                        $summon->pid = $blpid;
                        $summon->save();
                    }

                    /** if ($paid) {
                      $this->saveReceipt($request, $blpid, $paid);
                      } * */
                    return view("addsuccess", ['prid' => "blpid $blpid"]);
                } else {
                    throw new Exception();
                }
            } else {
                throw new Exception('Property not found');
            }
        } catch (Exception $e) {
            if (!empty($e->getMessage())) {
                return back()->with('server_error', $e->getMessage());
            } else {
                return back()->with('server_error', 'An error was encountered creating record. Please try again later');
            }
        }
    }

    /**
     * Add Business permit
     * @param AddBupermitRequest request
     * @return View
     */
    public function addBupermit(AddBupermitRequest $request) {
        try {
            
            $user = $request->user();
            $district = $user->district;
            $prid = $request->prid;

            $prate = Prate::where([["prid", "=", $prid], ["district", "=", $district]])->first();
            if (!empty($prate)) {
                $ownerid = $prate->ownerid;
                $fee = doubleval($request->fee);

                /*                 * if ($appfee) {
                  if ($request->paid == $appfee) {
                  $paid = $appfee;
                  } else {
                  return back()->with('server_error', 'Incorrect amount');
                  }
                  }* */

                $bupid = $this->genBupermitId();
                $bupermit = new Bupermit();
                $bupermit->prid = $prid;
                $bupermit->bupid = $bupid;
                $bupermit->ownerid = $ownerid;
                $bupermit->name_of_business = json_encode($request->name);
                $bupermit->total = $fee;
                //$bupermit->paid = $paid;

                if ($bupermit->save()) {
                    /*                     * if ($paid) {
                      $this->saveReceipt($request, $bupid, $paid);
                      }* */
                    return view("addsuccess", ['prid' => "$bupid"]);
                } else {
                    throw new Exception();
                }
            } else {
                throw new Exception('Property not found');
            }
        } catch (Exception $e) {
            if (!empty($e->getMessage())) {
                return back()->with('server_error', $e->getMessage());
            } else {
                return back()->with('server_error', 'An error was encountered creating record. Please try again later');
            }
        }
    }

    /**
     * Add Temporary Structure permit
     * @param AddTspermitRequest request
     * @return View
     */
    public function addTspermit(AddTspermitRequest $request) {
        try {
            
            $owner = $this->saveOwner($request);
            $tspermit_owner_id = $owner["ownerid"];
            $user = auth()->user();


            if ($tspermit_owner_id) {
                $penalty = 0;

                /**
                  if ($appfee) {
                  if ($request->paid == $appfee) {
                  $paid = $appfee;
                  } else {
                  return back()->with('server_error', 'Incorrect amount');
                  }
                  }* */
                if ($request->has("suid")) {
                    $suid = $request->suid;
                    $summon = Summon::where([["suid", "=", $suid],
                        ["prid","=",$prid]])->whereYear("created_at", date("Y"))->whereNull("pid")->first();
                    if (!empty($summon)) {
                        $penalty = doubleval($summon->penalty);
                    } else {
                        throw new Exception('Summon not found');
                    }
                }

                $tspermit_id = $this->genTspermitId();
                $tspermit = new Tspermit();

                $prid = $request->prid;
                $prate = Prate::where("prid", "=", $prid)->first();

                if (!empty($prate)) {
                    $tspermit->prid = $prid;


                    if ($owner["owner_exists"]) {
                        $ptype = $request->eowner_ptype;
                        if ($ptype == "rented") {
                            if (!Prate::where([["prid", "=", $prid], ["ownerid", "=", $tspermit_owner_id]])->first()) {
                                $tspermit->property_type = "rented";
                                $request->file('eowner_property_scan')->storeAs('public/property', $tspermit_id . "_rented" . ".jpg");
                            } else {
                                throw new Exception('Owned property cannot be rented');
                            }
                        } else {
                            $tspermit->property_type = "owned";
                        }
                    } else {
                        $tspermit->property_type = "rented";
                        $request->file('property_scan')->storeAs('public/property', $tspermit_id . "_rented" . ".jpg");
                    }

                    $tspermit->tspid = $tspermit_id;
                    $tspermit->ownerid = $tspermit_owner_id;
                    /*                     * $tspermit->location = $request->location;
                      if (!empty($request->ghpost)) {
                      $tspermit->ghpost_code = $request->ghpost;
                      }* */
                    $tspermit->type_of_building = $request->type;
                    $tspermit->fee = $request->fee;
                    $tspermit->penalty = $penalty;
                    $tspermit->total = $request->fee + $penalty;
                    //$tspermit->paid = $paid;

                    //create temp permit
                    if ($tspermit->save()) {
                        if (isset($summon) && !empty($summon)) {
                            $summon->pid = $tspermit_id;
                            $summon->save();
                        }
                        /**
                          if ($paid) {
                          $this->saveReceipt($request, $tspermit_id, $paid);
                          }* */
                        return view("addsuccess", ['prid' => $tspermit_id]);
                    } else {
                        throw new Exception();
                    }
                } else {
                    throw new Exception('Property not found');
                }
            }
        } catch (Exception $e) {
            //delete new owner
            if (isset($owner) && !$owner["owner_exists"]) {
                $owner->delete();
                Storage::delete('public/profiles/' . $owner["ownerid"] . 'jpg');
            }
            Storage::delete('public/property/' . $tspermit_id . "_rented" . ".jpg");
            if (!empty($e->getMessage())) {
                return back()->with('server_error', $e->getMessage());
            } else {
                return back()->with('server_error', 'An error was encountered creating record. Please try again later');
            }
        }
    }

    /**
     * Add Signage permit
     * @param AddSpermitRequest request
     * @return View
     */
    public function addSpermit(AddSpermitRequest $request) {
        try {
            //$appfee = Appfee::where("type_of_permit", "=", "spermits")->first()->fee;
            $owner = $this->saveOwner($request);
            $spermit_owner_id = $owner["ownerid"];
            $user = auth()->user();
            $district = $user->district;


            if ($spermit_owner_id) {
                $penalty = 0;

                /*                 * if ($appfee) {
                  if ($request->paid == $appfee) {
                  $paid = $appfee;
                  } else {
                  return back()->with('server_error', 'Incorrect amount');
                  }
                  }* */

                if ($request->has("suid")) {
                    $suid = $request->suid;
                    $summon = Summon::where([["suid", "=", $suid],
                        ["prid","=",$prid]])->whereYear("created_at", date("Y"))->whereNull("pid")->first();
                    if (!empty($summon)) {
                        $penalty = doubleval($summon->penalty);
                    } else {
                        throw new Exception('Summon not found');
                    }
                }

                $spermit_id = $this->genSpermitId();

                $spermit = new Spermit();
                $spermit->spid = $spermit_id;

                $prid = $request->prid;
                $prate = Prate::where("prid", "=", $prid)->first();
                if (!empty($prate)) {
                    $spermit->prid = $prid;
                    if ($owner["owner_exists"]) {
                        $ptype = $request->eowner_ptype;
                        if ($ptype == "rented") {
                            if (!Prate::where([["prid", "=", $prid], ["ownerid", "=", $spermit_owner_id]])->first()) {
                                $spermit->property_type = "rented";
                                $request->file('eowner_property_scan')->storeAs('public/property', $spermit_id . "_rented" . ".jpg");
                            } else {
                                throw new Exception('Owned property cannot be rented');
                            }
                        } else {
                            $spermit->property_type = "owned";
                        }
                    } else {
                        $spermit->property_type = "rented";
                        $request->file('property_scan')->storeAs('public/property', $spermit_id . "_rented" . ".jpg");
                    }


                    $spermit->ownerid = $spermit_owner_id;
                    /*                     * $spermit->location = $request->location;
                      if (!empty($request->ghpost)) {
                      $spermit->ghpost_code = $request->ghpost;
                      }* */
                    $spermit->size_of_signage = $request->size;
                    $spermit->fee = $request->fee;
                    $spermit->penalty = $penalty;
                    $spermit->total = $request->fee + $penalty;
                    //$spermit->paid = $paid;
                    

                    //create sign permit
                    if ($spermit->save()) {
                        if (isset($summon) && !empty($summon)) {
                            $summon->pid = $spermit_id;
                            $summon->save();
                        }
                        /*                         * if ($paid > 0) {
                          $this->saveReceipt($request, $spermit_id, $paid);
                          }* */

                        return view("addsuccess", ['prid' => $spermit_id]);
                    } else {
                        throw new Exception();
                    }
                } else {
                    throw new Exception('Property not found');
                }
            }
        } catch (Exception $e) {
            //delete new owner
            if (isset($owner) && !$owner["owner_exists"]) {
                $owner->delete();
                Storage::delete('public/profiles/' . $owner["ownerid"] . 'jpg');
            }
            Storage::delete('public/property/' . $tspermit_id . "_rented" . ".jpg");
            if (!empty($e->getMessage())) {
                return back()->with('server_error', $e->getMessage());
            } else {
                return back()->with('server_error', 'An error was encountered creating record. Please try again later');
            }
        }
    }

    /**
     * Add Summons
     * @param AddSummonRequest request
     * @return View
     */
    public function addSummon(AddSummonRequest $request) {
        try {
            $user = $request->user();
            $district = $user->district;

            if (Prate::where([["prid", "=", $request->prid],
                            ["ownerid", "=", $request->ownerid]])->first()) {

                $summon_id = $this->genSummonId();

                $summon = new Summon();
                $summon->suid = $summon_id;
                $summon->prid = $request->prid;
                $summon->type_of_permit = $request->ptype;
                $summon->type_of_building = $request->type;
                $summon->stage_of_const = $request->stage;
                $summon->penalty = $request->penalty;
                $summon->district = $district;

                //create  summon
                if ($summon->save()) {
                    return view("addsuccess", ['prid' => $summon_id]);
                } else {
                    throw new Exception();
                }
            } else {
                throw new Exception('Property not found');
            }
        } catch (Exception $e) {
            if (!empty($e->getMessage())) {
                return back()->with('server_error', $e->getMessage());
            } else {
                return back()->with('server_error', 'An error was encountered creating record. Please try again later');
            }
        }
    }

    /**
     * Get existing owner details, property rates and summons
     * @param Request request
     * @return JSON
     */
    public function getOwnerPrates(Request $request) {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $id_type = $request->eid_type;
        $id_number = $request->eid_number;

        $owner = Owner::where([["id_type", "=", $id_type],
                        ["id_number", "=", $id_number]])->first();

        if (!empty($owner)) {

            $owner_details = ["id" => $owner->ownerid, "name" => $owner->name, "picture" => asset("storage/profiles/" . $owner->ownerid . ".jpg")
                , "email" => $owner->email, "occupation" => $owner->occupation, "phone" => $owner->phone, "remarks" => json_decode($owner->extra)->remarks];

            $prates_details = array();
            $prates = Prate::where([["ownerid", "=", $owner->ownerid],
                            ["district", "=", $district]])->get();

            foreach ($prates as $prate) {
                array_push($prates_details, ['prid' => $prate->prid, "location" => $prate->location, "ghpost" => $prate->ghpost_code, "size" => $prate->size, "fee" => $prate->total, "paid" => $prate->paid, "district" => $prate->district]);
            }

            $summons_details = array();
            if (!empty($prates_details)) {
                $summons = Summon::where([["district", "=", $district],
                                        ["type_of_permit", "=", $request->type],
                                        ["prid", "=", $prates_details[0]["prid"]]])
                                ->whereYear("created_at", date("Y"))
                                ->whereNull("pid")->get();

                foreach ($summons as $summon) {
                    array_push($summons_details, ["suid" => $summon->suid, "type" => $summon->type_of_building, "stage" => $summon->stage_of_const, "penalty" => $summon->penalty]);
                }
            }

            return json_encode(["status" => 80, "owner" => $owner_details, "prates" => $prates_details, "summons" => $summons_details]);
        } else {
            return json_encode(["status" => 90]);
        }
    }

    /**
     * Get existing owner details and property rates
     * @param Request request
     * @return JSON
     */
    public function getOwnerPratesPost(Request $request) {
        $user = auth()->user();
        $district = $user->district;

        $ghpost = $request->ghpost;

        $prate = Prate::where([["ghpost_code", "=", $ghpost],
                        ["district", "=", $district]])->first();

        if (!empty($prate)) {

            $owner = $prate->owner()->first();
            $owner_details = ["id" => $owner->ownerid, "name" => $owner->name, "picture" => asset("storage/profiles/" . $owner->ownerid . ".jpg")
                , "email" => $owner->email, "occupation" => $owner->occupation, "phone" => $owner->phone, "remarks" => json_decode($owner->extra)->remarks];

            $prates_details = array();
            array_push($prates_details, ['prid' => $prate->prid, "location" => $prate->location, "ghpost" => $prate->ghpost_code, "size" => $prate->size, "district" => $prate->district]);

            return json_encode(["status" => 80, "owner" => $owner_details, "prates" => $prates_details]);
        } else {
            return json_encode(["status" => 90]);
        }
    }

    /**
     * Get property summons by property
     * @param Request request
     * @return JSON
     */
    public function getOwnerSummons(Request $request) {
        $user = auth()->user();
        $district = $user->district;

        $ownerid = $request->ownerid;
        $prid = $request->prid;
        $type = $request->type;

        $prate = Prate::where([["prid", "=", $prid],
                        ["ownerid", "=", $ownerid],
                        ["district", "=", $district]])->first();

        if (!empty($prate)) {

            $summons_details = array();

            $summons = Summon::where([["district", "=", $district],
                                    ["type_of_permit", "=", $type],
                                    ["prid", "=", $prid]])
                            ->whereYear("created_at", date("Y"))
                            ->whereNull("pid")->get();

            foreach ($summons as $summon) {
                array_push($summons_details, ["suid" => $summon->suid, "type" => $summon->type_of_building, "stage" => $summon->stage_of_const, "penalty" => $summon->penalty]);
            }


            return json_encode(["status" => 80, "summons" => $summons_details]);
        } else {
            return json_encode(["status" => 90]);
        }
    }

    /**
     * Get existing properties by gh post code
     * @param Request request
     * @return JSON
     */
    public function getPrates(Request $request) {
        $user = auth()->user();
        $district = $user->district;

        $ghpost_code = $request->prates_ghpost;
        $ownerid = $request->ownerid;
        $ptype = $request->ptype;
        $type = $request->type;

        if ($ptype == "owned") {
            if (Owner::where("ownerid", "=", $ownerid)->first()) {
                if ($ghpost_code) {
                    $prates = Prate::where([["ghpost_code", "LIKE", "%$ghpost_code%"],
                                    ["ownerid", "=", $ownerid]])->get();
                } else {
                    $prates = Prate::where("ownerid", "=", $ownerid)->get();
                }
            } else {
                return json_encode(["status" => 90]);
            }
        } else {
            $prates = Prate::where("ghpost_code", "=", $ghpost_code)->get();
        }

        if (!$prates->isEmpty()) {
            $prates_details = array();
            foreach ($prates as $prate) {
                $owner = $prate->owner()->first();
                array_push($prates_details, ["prid" => $prate->prid, "name" => $owner->name, "location" => $prate->location, "size" => $prate->size, "district" => $prate->district, "ghpost" => $prate->ghpost_code]);
            }


            $summons_details = array();
            if (!empty($prates_details)) {
                $summons = Summon::where([["district", "=", $district],
                                        ["type_of_permit", "=", $type],
                                        ["prid", "=", $prates_details[0]["prid"]]])
                                ->whereYear("created_at", date("Y"))
                                ->whereNull("pid")->get();

                foreach ($summons as $summon) {
                    array_push($summons_details, ["suid" => $summon->suid, "type" => $summon->type_of_building, "stage" => $summon->stage_of_const, "penalty" => $summon->penalty]);
                }
            }

            return json_encode(["status" => 80, "prates" => $prates_details, "summons" => $summons_details]);
        } else {
            return json_encode(["status" => 90]);
        }
    }
    
    
    
    
    public function getSummons(Request $request) {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $owner_name = $request->name;
        $owner_phone = $request->phone;

        $summons_details = array();

        if (!empty($owner_name)) {
            $summons = Summon::where([["district", "=", $district],
                                    ["type_of_permit", "=", $request->type]])
                            ->whereYear("created_at", date("Y"))
                            ->where(function($query) use ($owner_name, $owner_phone) {
                                foreach (explode(" ", $owner->name) as $name) {
                                    $query = $query->orWhere("name", "LIKE", "%$name%");
                                }
                                if ($owner_phone) {
                                    $phone = $owner->phone;
                                }
                                $query->orWhere("phone", "LIKE", "%$phone%");
                            })->whereNull("pid")->get();

            foreach ($summons as $summon) {
                array_push($summons_details, ["suid" => $summon->suid, "name" => $summon->name, "location" => $summon->location, "ghpost" => $summon->ghpost_code,
                    "type" => $summon->type_of_building, "stage" => $summon->stage_of_const, "remarks" => $summon->remarks, "penalty" => $summon->penalty]);
            }
        }
        return json_encode($summons_details);
    }

    /**
     * Save new owner or return existing user
     * @param Request request
     * @return array ["ownerid"=>,"owner_exists"=>]
     */
    public function saveOwner($request) {
        try {
            $exists = FALSE;
            $owner_id = "";

            //Check if existing user.TODO:consider removing check for eid fields
            if (!empty($request->eid_type) && !empty($request->eid_number) && !empty($request->ownerid)) {
                $eowner = Owner::where("ownerid", "=", $request->ownerid)->first();
                if ($eowner) {
                    $owner_id = $eowner->ownerid;
                    $exists = TRUE;
                } else {
                    throw new Exception('Invalid credentials');
                }
            } else {
                $name = $request->surname . " " . $request->first_name;
                $occupation = $request->occupation;
                $email = $request->email;
                $phone = $request->phone;
                $id_type = $request->id_type;
                $id_number = $request->id_number;
                $kin_name = $request->kin_name;
                $kin_phone = $request->kin_phone;
                $remarks = json_encode(["remarks" => $request->remarks]);

                $ownerid = $this->genOwnerId();

                $owner = new Owner();
                $owner->ownerid = $ownerid;
                $owner->name = $name;
                $owner->occupation = $occupation;
                $owner->email = $email;
                $owner->phone = $phone;
                $owner->id_type = $id_type;
                $owner->id_number = $id_number;
                $owner->kin_name = $kin_name;
                $owner->kin_phone = $kin_phone;
                $owner->extra = $remarks;

                //create owner
                if ($owner->save()) {
                    $owner_id = $ownerid;
                    $path = $request->file('id_scan')->storeAs('public/profiles', "$ownerid" . ".jpg"); //force to jpg
                } else {
                    throw new Exception('An error was encountered saving owner. Please try again later');
                }
            }

            return ["ownerid" => $owner_id, "owner_exists" => $exists];
        } catch (Exception $e) {
            throw $e;
        }
    }

    
     /**
     * Save receipt
     * @param Request request
     * @param pid property id
     * @param amount amount paid 
     * @return boolean
     */
    public function saveReceipt(Request $request, $pid, $amount) {
        $receipt = new Receipt();
        $receipt->pid = $pid;
        $receipt->rid = $this->genReceiptId(); //gen code
        $receipt->paid_amount = $amount;
        $receipt->receipt_code = $request->receipt; //gen code
        if ($request->file('receipt_scan')->storeAs('public/receipts', $receipt->rid . ".jpg")) {
            return $receipt->save();
        } else {
            return FALSE;
        }
    }

    /**
     * Show add property rate form
     *
     * @return View
     */
    public function addPrateForm() {
        return view("addprate");
    }

    /**
     * Show add building permit form
     *
     * @return View
     */
    public function addBlpermitForm() {
        return view("addblpermit");
    }

    /**
     * Show add business permit form
     *
     * @return View
     */
    public function addBupermitForm() {
        return view("addbupermit");
    }

    /**
     * Show add Temp Structure permit form
     *
     * @return View
     */
    public function addTspermitForm() {
        return view('addtspermit');
    }

    /**
     * Show add Signage & Adverts permit form
     *
     * @return View
     */
    public function addSpermitForm() {
        return view('addspermit');
    }

    /**
     * Show add Summon form
     *
     * @return View
     */
    public function addSummonForm() {
        return view('addsummon');
    }

    /**
     * Id generators
     *
     */
    public function genOwnerId() {
        return uniqid();
    }

    public function genPrateId() {
        return uniqid();
    }

    public function genBlpermitId() {
        return uniqid();
    }

    public function genBupermitId() {
        return uniqid();
    }

    public function genTspermitId() {
        return uniqid();
    }

    public function genSpermitId() {
        return uniqid();
    }

    public function genSummonId() {
        return uniqid();
    }

    /**
     * Generate receipt id
     *
     * @return string
     */
    public function genReceiptId() {
        return uniqid();
    }

}
