<?php

namespace App\Http\Controllers\Dash;

use Illuminate\Http\Request;
use App\Http\Requests\PayPrateRequest;
use App\Http\Requests\PayBlpermitRequest;
use App\Http\Requests\PayBupermitRequest;
use App\Http\Requests\PaySpermitRequest;
use App\Http\Requests\PayTspermitRequest;
use App\Http\Controllers\Controller;
use App\Prate;
use App\Spermit;
use App\Tspermit;
use App\Receipt;
use App\Owner;
use App\Summon;
use App\Blpermit;
use App\Bupermit;

class PayController extends Controller {

    /**
     * Pay property rates
     * @param string $prid property rate id
     * @param Request $request post request
     * @return View
     */
    public function payPrate($prid, PayPrateRequest $request) {
        try {
            $user = auth()->user();
            $role = $user->role;
            $district = $user->district;
            $npaid = $request->paid;

            $prate = Prate::where([["prev_paid", "=", "0"],
                            ["district", "=", $district],
                            ["prid", "=", $prid]])->first();
            if (!empty($prate)) {
                $opaid = $prate->paid;
                $paid = $opaid + $npaid;

                if ($paid >= $prate->total) {
                    $prate->prev_paid = $prate->total;
                }
                $prate->paid = $paid;

                if ($prate->save()) {
                    if ($this->saveReceipt($request, $prate->prid, $npaid)) {
                        return view("paysuccess", ["prid" => $prate->prid]);
                    } else {
                        $prate->prev_paid = 0;
                        $prate->paid = $opaid;
                        $prate->save();
                        throw new Exception();
                    }
                }
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            if (!empty($e->getMessage())) {
                return back()->with('server_error', $e->getMessage());
            } else {
                return back()->with('server_error', 'An error was encountered during payment. Please try again later');
            }
        }
    }

    /**
     * Pay business permit
     * @param string $bupid business permit id
     * @param Request $request post request
     * @return View
     */
    public function payBupermit($bupid, PayBupermitRequest $request) {
        try {
            $user = auth()->user();
            $role = $user->role;
            $district = $user->district;
            $npaid = $request->paid;

            $bupermit = Bupermit::where([["bupermits.prev_paid", "=", "0"],
                            ["prates.district", "=", $district],
                            ["bupid", "=", $bupid]])
                    ->select("bupermits.total", "bupermits.paid", "bupermits.bupid", "bupermits.prev_paid", "bupermits.id")
                    ->join("prates", "bupermits.prid", "=", "prates.prid")
                    ->first();
            if (!empty($bupermit)) {
                $opaid = $bupermit->paid;
                $paid = $opaid + $npaid;

                if ($paid >= $bupermit->total) {
                    $bupermit->prev_paid = $bupermit->total;
                }
                $bupermit->paid = $paid;

                if ($bupermit->save()) {
                    if ($this->saveReceipt($request, $bupermit->bupid, $npaid)) {
                        return view("paysuccess");
                    } else {
                        $bupermit->prev_paid = 0;
                        $bupermit->paid = $opaid;
                        $bupermit->save();
                        throw new Exception();
                    }
                }
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            if (!empty($e->getMessage())) {
                return back()->with('server_error', $e->getMessage());
            } else {
                return back()->with('server_error', 'An error was encountered during payment. Please try again later');
            }
        }
    }

    /**
     * Pay building permit
     * @param string $tspid temp permit id
     * @param Request $request post request
     * @return View
     */
    public function payBlpermit($blpid, PayBlpermitRequest $request) {
        try {
            $user = auth()->user();
            $role = $user->role;
            $district = $user->district;
            $npaid = $request->paid;

            $blpermit = Blpermit::where([["blpermits.prev_paid", "=", "0"],
                            ["prates.district", "=", $district],
                            ["blpid", "=", $blpid]])
                    ->select("blpermits.total", "blpermits.paid", "blpermits.blpid", "blpermits.prev_paid", "blpermits.id", "blpermits.prid")
                    ->join("prates", "blpermits.prid", "=", "prates.prid")
                    ->first();
            if (!empty($blpermit)) {

                if ($request->has("suid") && !empty($request->suid) && $blpermit->paid == 0) {
                    $year = date('Y', strtotime($blpermit->created_at));
                    $summon = Summon::where([["district", "=", $district],
                                            ["suid", "=", $request->suid],
                                            ["prid", "=", $blpermit->prid]])
                                    ->whereYear("created_at", "=", $year)->first();
                    if (!empty($summon)) {
                        $fee = $summon->penalty + $blpermit->fee;
                        $blpermit->fee = $fee;
                    } else {
                        throw new Exception("Summon not found");
                    }
                }

                $opaid = $blpermit->paid;
                $paid = $opaid + $npaid;

                if ($paid >= $blpermit->total) {
                    $blpermit->prev_paid = $blpermit->total;
                }
                $blpermit->paid = $paid;

                if ($blpermit->save()) {
                    if (!empty($summon)) {
                        $summon->pid = $blpermit->blpid;
                        $summon->save();
                    }

                    if ($this->saveReceipt($request, $blpermit->blpid, $npaid)) {
                        return view("paysuccess");
                    } else {
                        $blpermit->prev_paid = 0;
                        $blpermit->paid = $opaid;
                        $blpermit->save();
                        $summon->pid = "NULL";
                        $summon->save();
                        throw new Exception();
                    }
                }
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            if (!empty($e->getMessage())) {
                return back()->with('server_error', $e->getMessage());
            } else {
                return back()->with('server_error', 'An error was encountered during payment. Please try again later');
            }
        }
    }

    /**
     * Pay signage permit
     * @param string $spid signage permit id
     * @param Request $request post request
     * @return View
     */
    public function paySpermit($spid, PaySpermitRequest $request) {
        try {
            $user = auth()->user();
            $role = $user->role;
            $district = $user->district;
            $npaid = $request->paid;

            $spermit = Spermit::where([["spermits.prev_paid", "=", "0"],
                            ["prates.district", "=", $district],
                            ["spermits.spid", "=", $spid]])
                    ->select("spermits.total", "spermits.paid", "spermits.spid", "spermits.prev_paid", "spermits.id", "spermits.prid")
                    ->join("prates", "spermits.prid", "=", "prates.prid")
                    ->first();
            if (!empty($spermit)) {

                if ($request->has("suid") && !empty($request->suid) && $spermit->paid == 0) {
                    $year = date('Y', strtotime($spermit->created_at));
                    $summon = Summon::where([["district", "=", $district],
                                            ["suid", "=", $request->suid],
                                            ["prid", "=", $spermit->prid]])
                                    ->whereYear("created_at", "=", $year)->first();
                    if (!empty($summon)) {
                        $fee = $summon->penalty + $spermit->fee;
                        $spermit->fee = $fee;
                    } else {
                        throw new Exception("Summon not found");
                    }
                }

                $opaid = $spermit->paid;
                $paid = $opaid + $npaid;

                if ($paid >= $spermit->total) {
                    $spermit->prev_paid = $spermit->total;
                }
                $spermit->paid = $paid;

                if ($spermit->save()) {
                    if (!empty($summon)) {
                        $summon->pid = $spermit->spid;
                        $summon->save();
                    }

                    if ($this->saveReceipt($request, $spermit->spid, $npaid)) {
                        return view("paysuccess");
                    } else {
                        $spermit->prev_paid = 0;
                        $spermit->paid = $opaid;
                        $spermit->save();
                        $summon->pid = "NULL";
                        $summon->save();
                        throw new Exception();
                    }
                }
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            if (!empty($e->getMessage())) {
                return back()->with('server_error', $e->getMessage());
            } else {
                return back()->with('server_error', 'An error was encountered during payment. Please try again later');
            }
        }
    }

    /**
     * Pay temporary structure permit
     * @param string $tspid temp permit id
     * @param Request $request post request
     * @return View
     */
    public function payTspermit($tspid, PayTspermitRequest $request) {
        try {
            $user = auth()->user();
            $role = $user->role;
            $district = $user->district;
            $npaid = $request->paid;

            $tspermit = Tspermit::where([["tspermits.prev_paid", "=", "0"],
                            ["prates.district", "=", $district],
                            ["tspermits.tspid", "=", $tspid]])
                    ->select("tspermits.total", "tspermits.paid", "tspermits.tspid", "tspermits.prev_paid", "tspermits.id", "tspermits.prid")
                    ->join("prates", "tspermits.prid", "=", "prates.prid")
                    ->first();
            if (!empty($tspermit)) {

                if ($request->has("suid") && !empty($request->suid) && $tspermit->paid == 0) {
                    $year = date('Y', strtotime($tspermit->created_at));
                    $summon = Summon::where([["district", "=", $district],
                                            ["suid", "=", $request->suid],
                                            ["prid", "=", $spermit->prid]])
                                    ->whereYear("created_at", "=", $year)->first();
                    if (!empty($summon)) {
                        $fee = $summon->penalty + $tspermit->fee;
                        $tspermit->fee = $fee;
                    } else {
                        throw new Exception("Summon not found");
                    }
                }
                $opaid = $tspermit->paid;
                $paid = $opaid + $npaid;

                if ($paid >= $tspermit->total) {
                    $tspermit->prev_paid = $tspermit->total;
                }
                $tspermit->paid = $paid;

                if ($tspermit->save()) {
                    if (!empty($summon)) {
                        $summon->pid = $tspermit->tspid;
                        $summon->save();
                    }

                    if ($this->saveReceipt($request, $tspermit->tspid, $npaid)) {
                        return view("paysuccess");
                    } else {
                        $tspermit->prev_paid = 0;
                        $tspermit->paid = $opaid;
                        $tspermit->save();
                        $summon->pid = "NULL";
                        $summon->save();
                        throw new Exception();
                    }
                }
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            if (!empty($e->getMessage())) {
                return back()->with('server_error', $e->getMessage());
            } else {
                return back()->with('server_error', 'An error was encountered during payment. Please try again later');
            }
        }
    }

    /**
     * Show property rates pay form
     * @param string $prid property rate id
     * @return View
     */
    public function payPrateForm($prid) {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $prate = Prate::where([["prev_paid", "=", "0"],
                        ["district", "=", $district],
                        ["prid", "=", $prid]])->first();
        if (!empty($prate)) {
            $owner = $prate->owner()->first();
            return view("payprate", ["prate" => $prate, "owner" => $owner]);
        } else {
            abort(404);
        }
    }

    /**
     * Show Business permit pay form
     * @param string $bupid business id
     * @return View
     */
    public function payBupermitForm($bupid) {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $bupermit = Bupermit::where([["bupermits.prev_paid", "=", "0"],
                        ["prates.district", "=", $district],
                        ["bupid", "=", $bupid]])
                ->select("bupermits.name_of_business", "bupermits.total", "bupermits.paid", "bupermits.bupid", "bupermits.prid", "bupermits.ownerid")
                ->join("prates", "bupermits.prid", "=", "prates.prid")
                ->first();
        if (!empty($bupermit)) {
            $owner = $bupermit->owner()->first();
            $prate = $bupermit->prate()->first();

            $result = ["bupermit" => $bupermit, "owner" => $owner, "prate" => $prate];
            return view("paybupermit", $result);
        } else {
            abort(404);
        }
    }

    /**
     * Show Building permit pay form
     * @param string $blpid property rate id
     * @return View
     */
    public function payBlpermitForm($blpid) {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $blpermit = Blpermit::where([["blpermits.prev_paid", "=", "0"],
                        ["prates.district", "=", $district],
                        ["blpid", "=", $blpid]])
                ->select("blpermits.type_of_building", "blpermits.total", "blpermits.paid", "blpermits.blpid", "blpermits.prid", "blpermits.ownerid")
                ->join("prates", "blpermits.prid", "=", "prates.prid")
                ->first();
        if (!empty($blpermit)) {
            $summon = $blpermit->summon()->first();
            $owner = $blpermit->owner()->first();
            $prate = $blpermit->prate()->first();

            $result = ["blpermit" => $blpermit, "owner" => $owner, "prate" => $prate];
            if (!empty($summon)) {
                $result['summon'] = $summon;
            } elseif ($blpermit->paid == 0) {
                $year = date('Y', strtotime($blpermit->created_at));
                $nsummons = Summon::where([["district", "=", $district],
                                        ["type_of_permit", "=", "building"],
                                        ["prid", "=", $blpermit->prid]])
                                ->whereYear("created_at", date("Y"))
                                ->whereNull("pid")->get();
                if (!$nsummons->isEmpty()) {
                    $result['nsummons'] = $nsummons;
                }
            }
            return view("payblpermit", $result);
        } else {
            abort(404);
        }
    }

    /**
     * Show temp permit pay form
     * @param string $tspid temp permit id
     * @return View
     */
    public function payTspermitForm($tspid) {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $tspermit = Tspermit::where([["tspermits.prev_paid", "=", "0"],
                        ["prates.district", "=", $district],
                        ["tspid", "=", $tspid]])
                ->select("tspermits.type_of_building", "tspermits.property_type", "tspermits.total", "tspermits.paid", "tspermits.tspid", "tspermits.prid", "tspermits.ownerid")
                ->join("prates", "tspermits.prid", "=", "prates.prid")
                ->first();
        if (!empty($tspermit)) {
            $summon = $tspermit->summon()->first();
            $owner = $tspermit->owner()->first();
            $prate = $tspermit->prate()->first();

            $result = ["tspermit" => $tspermit, "owner" => $owner, "prate" => $prate];
            if (!empty($summon)) {
                $result['summon'] = $summon;
            } elseif ($tspermit->paid == 0) {
                $year = date('Y', strtotime($tspermit->created_at));
                $nsummons = Summon::where([["district", "=", $district],
                                        ["type_of_permit", "=", "temp"],
                                        ["prid", "=", $tspermit->prid]])
                                ->whereYear("created_at", date("Y"))
                                ->whereNull("pid")->get();
                if (!$nsummons->isEmpty()) {
                    $result['nsummons'] = $nsummons;
                }
            }
            return view("paytspermit", $result);
        } else {
            abort(404);
        }
    }

    /**
     * Show Signage permit pay form
     * @param string $spid property rate id
     * @return View
     */
    public function paySpermitForm($spid) {
        $user = auth()->user();
        $role = $user->role;
        $district = $user->district;

        $spermit = Spermit::where([["spermits.prev_paid", "=", "0"],
                                ["prates.district", "=", $district],
                                ["spid", "=", $spid]])
                        ->select("spermits.size_of_signage", "spermits.property_type", "spermits.total", "spermits.paid", "spermits.spid", "spermits.prid", "spermits.ownerid")
                        ->join("prates", "spermits.prid", "=", "prates.prid")->first();
        if (!empty($spermit)) {
            $summon = $spermit->summon()->first();
            $owner = $spermit->owner()->first();
            $prate = $spermit->prate()->first();

            $result = ["spermit" => $spermit, "owner" => $owner, "prate" => $prate];
            if (!empty($summon)) {
                $result['summon'] = $summon;
            } elseif ($spermit->paid == 0) {
                $year = date('Y', strtotime($spermit->created_at));
                $nsummons = Summon::where([["district", "=", $district],
                                        ["type_of_permit", "=", "signage"],
                                        ["prid", "=", $spermit->prid]])
                                ->whereYear("created_at", date("Y"))
                                ->whereNull("pid")->get();
                if (!$nsummons->isEmpty()) {
                    $result['nsummons'] = $nsummons;
                }
            }
            return view("payspermit", $result);
        } else {
            abort(404);
        }
    }

    public function saveReceipt(Request $request, $pid, $amount) {
        $receipt = new Receipt();
        $receipt->pid = $pid;
        $receipt->rid = $this->genReceiptId(); //gen code
        $receipt->paid_amount = $amount;
        $receipt->receipt_code = $request->receipt; //gen code
        if ($request->file('receipt_scan')->storeAs('public/receipts', $receipt->rid . ".jpg")) {
            return $receipt->save();
        } else {
            return FALSE;
        }
    }

    /**
     * Generate receipt id
     *
     * @return string
     */
    public function genReceiptId() {
        return uniqid();
    }

}
