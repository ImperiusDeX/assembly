<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddBupermitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "prid"=>"required|string",
            "name"=>"required|array",           
            "fee"=>"required|numeric",
            //"paid"=>"required|numeric",
            //"receipt"=>"required_unless:paid,0|nullable|string|unique:receipts,receipt_code",
            //"receipt_scan"=>"required_unless:paid,0|nullable|mimetypes:image/jpeg"
            ];
    }
       
    public function messages()
    {
        return [
            "prid.required"=>"Valid property is required",
            "prid.string"=>"Valid property is required",
            "name.required"=>"Business name(s) is required",
            "name.string"=>"Business name(s) must be an array",
            "fee.required"=>"Permit fee is required",
            "fee.numeric"=>"Permit fee should be numeric"];
    }
}
