<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaySpermitRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "paid" => "required|numeric",
            "receipt" => "required|string|unique:receipts,receipt_code",
            "receipt_scan" => "required|mimetypes:image/jpeg"
        ];
    }
    
    
    
    public function messages() {
        return [
            "paid.required"=>"Paid is required",
            "paid.numeric"=>"Paid should be numeric",
            "receipt.required"=>"Receipt code is required",
            "receipt.unique"=>"This receipt code already exists",
            "receipt_scan.required"=>"Receipt scan is required",
            "receipt_scan.mimetypes"=>"Receipt scan should be a jpeg/jpg file"
        ];
    }

}
