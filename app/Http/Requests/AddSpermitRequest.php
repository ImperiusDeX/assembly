<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;

class AddSpermitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules=[           
            "surname"=>"required_without:ownerid|regex:/^[a-z\s_-]+$/i|nullable",
            "first_name"=>"required_without:ownerid|regex:/^[a-z\s_-]+$/i|nullable",        
            "occupation"=>"required_without:ownerid|alpha|nullable",
            "email"=>"required_without:ownerid|email|unique:owners|nullable",
            "phone"=>"required_without:ownerid|unique:owners|digits_between:8,11|nullable",
            "id_type"=>"required_without:ownerid|string|in:drivers,passport,nid",
            "id_number"=>"required_without:ownerid|unique:owners|nullable",
            "kin_name"=>"required_without:ownerid|regex:/^[a-z\s_-]+$/i|string|nullable",
            "kin_phone"=>"required_without:ownerid|digits_between:8,11|nullable",
            "remarks"=>"string|nullable",
            "size"=>"required|string",
            //"ghpost"=>"required|unique:spermits,ghpost_code|string",
            //"location"=>"required_without:ownerid|string|nullable",
            "suid"=>"string|nullable",
            "fee"=>"required|numeric",
            //"paid"=>"required|numeric",
            //"receipt"=>"required_unless:paid,0|nullable|string|unique:receipts,receipt_code",
            //"receipt_scan"=>"required_unless:paid,0|nullable|mimetypes:image/jpeg",
            "ownerid"=>"string|nullable",
            "prid"=>"required",
            "id_scan"=>"required_without:ownerid|mimetypes:image/jpeg|nullable"
        ];
        
        if(!empty(Input::get("ownerid"))){
            $rules["eowner_ptype"]="required_with:ownerid|in:rented,owned";
            $rules['eowner_property_scan']="required_if:eowner_ptype,rented|mimetypes:image/jpeg";
        }else{
            $rules["ptype"]="required_without:ownerid|in:rented";
            $rules["property_scan"]="required_without:ownerid|mimetypes:image/jpeg";
        }
        
        return $rules; 
    }
    
    
    
    public function messages()
    {
        return [
            "surname.required_without"=>"Surname is required",
            "surname.regex"=>"Surname should contain only letters",
            "first_name.required_without"=>"First name is required", 
            "first_name.regex"=>"First name should contain only letters", 
            "occupation.required_without"=>"Occupation is required",
            "occupation.alpha"=>"Occupation should contain only letters",
            "email.required_without"=>"Email is required",
            "email.email"=>"Email should be a valid email address",
            "email.unique"=>"This email address already exists",
            "phone.required_without"=>"Phone number is required",
            "phone.unique"=>"Phone number already exists",
            "phone.digits_between"=>"Phone number should be between 9-10 digits",
            "id_type.required_without"=>"ID type is required",
            "id_type.string"=>"ID type should be a string",
            "id_type.in"=>"ID type is invalid",
            "id_number.required_without"=>"ID number is required",
            "id_number.unique"=>"This ID number already exists",
            "kin_name.required_without"=>"Name of the next of kin is required",
            "kin_name.regex"=>"Name of the next of kin should only contain letters",
            "kin_phone.required_without"=>"Phone number of the next of kin is required",
            "kin_phone.digits_between"=>"Phone number of the next of kin should be between 9-10 digits",
            "size.required"=>"Signage size is required",
            "fee.required"=>"Permit fee is required",
            "fee.numeric"=>"The permit fee should be numeric",
            "id_scan.required_without"=>"ID scan is required",
            "id_scan.mimetypes"=>"ID scan should be a jpeg/jpg file",
            "eowner_ptype.required_with"=>"Property type is required",
            "eowner_ptype.in"=>"Property type is invalid",
            "eowner_property_scan.required_if"=>"Documentation is required for rented property",
            "eowner_property_scan.mimetypes"=>"Documentation is should be jpeg/jpg file",
            "ptype.required_without"=>"Property type is required",
            "ptype.in"=>"Property type is invalid",
            "property_scan.required_without"=>"Documentation is required for rented property",
            "property_scan.mimetypes"=>"Documentation is should be jpeg/jpg file",
            ];
    }
}