<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPrateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules=[
            "surname"=>"required_without:ownerid|regex:/^[a-z\s_-]+$/i|nullable",
            "first_name"=>"required_without:ownerid|regex:/^[a-z\s_-]+$/i|nullable",       
            "occupation"=>"required_without:ownerid|alpha|nullable",
            "email"=>"required_without:ownerid|email|unique:owners|nullable",
            "phone"=>"required_without:ownerid|unique:owners|digits_between:8,11|nullable",
            "id_type"=>"required_without:ownerid|string|in:drivers,passport,nid",
            "id_number"=>"required_without:ownerid|unique:owners|nullable",
            "kin_name"=>"required_without:ownerid|regex:/^[a-z\s_-]+$/i|string|nullable",
            "kin_phone"=>"required_without:ownerid|digits_between:8,11|nullable",
            "remarks"=>"string|nullable",
            "location"=>"required|string",
            "ghpost"=>"required|unique:prates,ghpost_code|string",
            "size"=>"required|string",
            "fee"=>"required|numeric",
            //"paid"=>"required|numeric",
            //"receipt"=>"required_unless:paid,0|nullable|string|unique:receipts,receipt_code",
            //"receipt_scan"=>"required_unless:paid,0|nullable|mimetypes:image/jpeg",
            "ownerid"=>"string|nullable",
            "id_scan"=>"required_without:ownerid|mimetypes:image/jpeg|nullable"
        ];
        
        return $rules;
    }
    
    
    public function messages()
    {
        return [
            "surname.required_without"=>"Surname is required",
            "surname.regex"=>"Surname should contain only letters",
            "first_name.required_without"=>"First name is required", 
            "first_name.regex"=>"First name should contain only letters", 
            "occupation.required_without"=>"Occupation is required",
            "occupation.alpha"=>"Occupation should contain only letters",
            "email.required_without"=>"Email is required",
            "email.email"=>"Email should be a valid email address",
            "email.unique"=>"This email address already exists",
            "phone.required_without"=>"Phone number is required",
            "phone.unique"=>"Phone number already exists",
            "phone.digits_between"=>"Phone number should be between 9-10 digits",
            "id_type.required_without"=>"ID type is required",
            "id_type.string"=>"ID type should be a string",
            "id_type.in"=>"ID type is invalid",
            "id_number.required_without"=>"ID number is required",
            "id_number.unique"=>"ID number already exists",
            "kin_name.required_without"=>"Name of the next of kin is required",
            "kin_name.regex"=>"Name of the next of kin should only contain letters",
            "kin_phone.required_without"=>"Phone number of the next of kin is required",
            "kin_phone.digits_between"=>"Phone number of the next of kin should be between 9-10 digits",
            "location.required"=>"Location is required",
            "ghpost.required"=>"GH Post Code is required",
            "ghpost.unique"=>"This GH Post Code already exists",
            "size.required"=>"Property size is required",
            "fee.required"=>"Permit fee is required",
            "fee.numeric"=>"Permit fee should be numeric",
            "id_scan.required_without"=>"ID scan is required",
            "id_scan.mimetypes"=>"ID scan should be a jpeg/jpg file",
            ];
    }
}
