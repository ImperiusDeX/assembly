<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddSummonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "ownerid"=>"required",
            "prid"=>"required",
            "ptype"=>"required|in:building,temp,signage",
            "type"=>"required|string",
            "stage"=>"required|string",
            "penalty"=>"required|numeric"
        ];
    }
    
    public function messages(){
        return [
            "ownerid.required"=>"Valid property is required",
            "prid.required"=>"Valid property is required",
            "ptype.required"=>"Property type is required",
            "ptype.in"=>"Property type is invalid",
            "stage.required"=>"Stage of construction is required",
            "penalty.required"=>"Penalty is required",
            "penalty.numeric"=>"Penalty should be numeric"
        ];
    }
}
