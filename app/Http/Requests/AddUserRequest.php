<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "surname"=>"required|regex:/^[a-z\s_-]+$/i",
            "first_name"=>"required|regex:/^[a-z\s_-]+$/i",
            "email"=>"required|email|unique:users",
            "phone"=>"required|unique:users|digits_between:8,11",
            "prate"=>"array",
            "blpermit"=>"array",
            "bupermit"=>"array",
            "spermit"=>"array",
            "tspermit"=>"array",
            "summon"=>"array",
            "password"=>"required|confirmed"
        ];
    }
    
    
    public function messages() {
        return [
            "surname.required"=>"Surname is required",
            "surname.regex"=>"Surname should contain only letters",
            "first_name.required_without"=>"First name is required", 
            "first_name.regex"=>"First name should contain only letters",
            "email.required"=>"Email is required",
            "email.email"=>"Email should be a valid email address",
            "email.unique"=>"This email address already exists",
            "phone.required"=>"Phone number is required",
            "phone.unique"=>"Phone number already exists",
            "phone.digits_between"=>"Phone number should be between 9-10 digits",
            "password.required"=>"Password is required",
            "password.confirmed"=>"The passwords do not match"
        ];
    }
}
