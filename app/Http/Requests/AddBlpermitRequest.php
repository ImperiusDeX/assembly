<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddBlpermitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "prid"=>"required|string",
            "type"=>"required|string",           
            "fee"=>"required|numeric",
            //"paid"=>"required|numeric",
            //"receipt"=>"required_unless:paid,0|nullable|string|unique:receipts,receipt_code",
            //"receipt_scan"=>"required_unless:paid,0|nullable|mimetypes:image/jpeg",
            "suid"=>"string|nullable"];
    }
    
    
    public function messages()
    {
        return [
            "prid.required"=>"Please provide a property",
            "prid.string"=>"Property ID must be string",
            "type.required"=>"Type of building is required",
            "type.string"=>"The type of building must be a string",
            "fee.required"=>"Permit fee is required",
            "fee.numeric"=>"The permit fee must be numeric",
            "suid.string"=>"Summon ID must be string"];
    }
    
    
    
}
