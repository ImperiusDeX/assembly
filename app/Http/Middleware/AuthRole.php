<?php

namespace App\Http\Middleware;

use Closure;

class AuthRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$section,$permission)
    {
        if($request->user()->hasRole($section,$permission)){
            return $next($request);
        }else{
            abort(404);
        }
    }
}
