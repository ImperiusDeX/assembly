<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bupermit extends Model
{
    /*
     * Get permit owner
     * @returns App\Owner
     */
    public function owner(){
        return $this->belongsTo("App\Owner","ownerid","ownerid");
    }
    
    
    /*
     * Get related building permit
     * @returns App\Blpermit
     */
    public function blpermit(){
        return $this->belongsTo("App\blpermit","blpid");
    }
    
    /*
     * Get related property rate record
     * @returns App\Prate
     */
    public function prate(){
        return $this->belongsTo("App\Prate","prid","prid");
    }
    
    /*
     * Get receipts
     * @returns App\Receipt
     */
    public function receipts(){
        return $this->hasMany("App\Receipt","pid","bupid");
    }
}
