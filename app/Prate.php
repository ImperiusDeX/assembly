<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prate extends Model
{
    /*
     * Get permit owner
     * @returns App\Owner
     */
    public function owner(){
        return $this->belongsTo("App\Owner","ownerid","ownerid");
    }
    
    
    /*
     * Get related bulding permit
     * @returns App\Blpermit
     */
    public function blpermit(){
        return $this->hasMany("App\Blpermit","prid","prid");
    }
    
    /*
     * Get receipts
     * @returns App\Receipt
     */
    public function receipts(){
        return $this->hasMany("App\Receipt","pid","prid");
    }
    
    /*
     * Get related summons
     * @returns App\Summon
     */
    public function summon(){
        return $this->hasMany("App\Summon","prid","prid");
    }
}
