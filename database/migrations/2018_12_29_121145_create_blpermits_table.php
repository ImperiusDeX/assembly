<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlpermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blpermits', function (Blueprint $table) {
            $table->increments('id');
            $table->string("blpid");
            $table->string("prid");
            $table->string("ownerid");
            $table->string("type_of_building");
            $table->decimal("fee",30,2);
            $table->decimal("penalty",30,2)->default(0);
            $table->decimal("total",30,2);
            $table->decimal("paid",30,2)->default(0);
            $table->decimal("prev_paid",30,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blpermits');
    }
}
 