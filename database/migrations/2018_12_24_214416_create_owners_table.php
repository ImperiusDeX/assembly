<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owners', function (Blueprint $table) {
            $table->increments('id');
            $table->string("ownerid");
            $table->string('name');
            $table->string('occupation');
            $table->string('phone');
            $table->string("email")->unique();
            $table->string("id_type");
            $table->string("id_number")->unique();
            $table->string("kin_name");
            $table->string("kin_phone");
            $table->json("extra")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owners');
    }
}
