<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBupermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bupermits', function (Blueprint $table) {
            $table->increments('id');
            $table->string("prid");
            $table->string("bupid");
            $table->string("ownerid");
            $table->json("name_of_business"); //json array
            $table->decimal("total",30,2);
            $table->decimal("paid",30,2)->default(0);
            $table->decimal("prev_paid",30,2)->default(0); //the previous fully paid for fee
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bupermits');
    }
}
