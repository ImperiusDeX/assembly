<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePratesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prates', function (Blueprint $table) {
            $table->increments('id');
            $table->string("prid");
            $table->string("ownerid");
            $table->string("location");
            $table->string("ghpost_code");
            $table->string("size");
            $table->decimal("total",30,2);
            $table->decimal("paid",30,2)->default(0);
            $table->decimal("prev_paid",30,2)->default(0); //the previous fully paid for fee
            $table->string("district");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prates');
    }
}
