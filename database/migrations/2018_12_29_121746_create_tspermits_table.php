<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTspermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tspermits', function (Blueprint $table) {
            $table->increments('id');
            $table->string("tspid");
            $table->string("prid");
            $table->string("ownerid");
            $table->enum("property_type",["owned","rented"]);
            $table->string("type_of_building");
            $table->decimal("fee",30,2);
            $table->decimal("penalty",30,2)->default(0);
            $table->decimal("total",30,2);
            $table->decimal("paid",30,2)->default(0);
            $table->decimal("prev_paid",30,2)->default(0); //the previous fully paid for fee
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tspermits');
    }
}
