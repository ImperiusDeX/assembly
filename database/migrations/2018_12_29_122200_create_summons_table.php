<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSummonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summons', function (Blueprint $table) {
            $table->increments('id');
            $table->string("suid");
            $table->string("pid")->nullable();
            $table->string("prid");
            $table->enum("type_of_permit",['building','temp','signage']);
            $table->string("type_of_building");
            $table->string("stage_of_const");
            $table->decimal("penalty",30,2);
            $table->string("district");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summons');
    }
}
