<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */


Auth::routes();

Route::group(['middleware' => ['auth']], function() {

    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');


    Route::get('/due/prate/', 'Dash\DueController@duePrates')->name('duePrates')->middleware("auth.roles:1,1");
    Route::get('/due/blpermit/', 'Dash\DueController@dueBlpermits')->name('dueBlpermits')->middleware("auth.roles:2,1");
    Route::get('/due/bupermit/', 'Dash\DueController@dueBupermits')->name('dueBupermits')->middleware("auth.roles:3,1");
    Route::get('/due/spermit/', 'Dash\DueController@dueSpermits')->name('dueSpermits')->middleware("auth.roles:4,1");
    Route::get('/due/tspermit/', 'Dash\DueController@dueTspermits')->name('dueTspermits')->middleware("auth.roles:5,1");

    Route::get('/pay/prate/{prid}/', 'Dash\PayController@payPrateForm')->name('payPrateForm')->middleware("auth.roles:1,1");
    Route::post('/pay/prate/{prid}/', 'Dash\PayController@payPrate')->name('payPrate')->middleware("auth.roles:1,1");
    Route::get('/pay/blpermit/{blpid}/', 'Dash\PayController@payBlpermitForm')->name('payBlpermitForm')->middleware("auth.roles:2,1");
    Route::post('/pay/blpermit/{blpid}/', 'Dash\PayController@payBlpermit')->name('payBlpermit')->middleware("auth.roles:2,1");
    Route::get('/pay/bupermit/{bupid}/', 'Dash\PayController@payBupermitForm')->name('payBupermitForm')->middleware("auth.roles:3,1");
    Route::post('/pay/bupermit/{bupid}/', 'Dash\PayController@payBupermit')->name('payBupermit')->middleware("auth.roles:3,1");
    Route::get('/pay/spermit/{spid}/', 'Dash\PayController@paySpermitForm')->name('paySpermitForm')->middleware("auth.roles:4,1");
    Route::post('/pay/spermit/{spid}/', 'Dash\PayController@paySpermit')->name('paySpermit')->middleware("auth.roles:4,1");
    Route::get('/pay/tspermit/{tspid}/', 'Dash\PayController@payTspermitForm')->name('payTspermitForm')->middleware("auth.roles:5,1");
    Route::post('/pay/tspermit/{tspid}/', 'Dash\PayController@payTspermit')->name('payTspermit')->middleware("auth.roles:5,1");

    Route::get('/add/prate', 'Dash\AddController@addPrateForm')->name('addPrateForm')->middleware("auth.roles:1,2");
    Route::post('/add/prate', 'Dash\AddController@addPrate')->name('addPrate')->middleware("auth.roles:1,2");
    Route::get('/add/blpermit/', 'Dash\AddController@addBlpermitForm')->name('addBlpermitForm')->middleware("auth.roles:2,2");
    Route::post('/add/blpermit', 'Dash\AddController@addBlpermit')->name('addBlpermit')->middleware("auth.roles:2,2");
    Route::get('/add/bupermit/', 'Dash\AddController@addBupermitForm')->name('addBupermitForm')->middleware("auth.roles:3,2");
    Route::post('/add/bupermit', 'Dash\AddController@addBupermit')->name('addBupermit')->middleware("auth.roles:3,2");
    Route::get('add/spermit', 'Dash\AddController@addSpermitForm')->name('addSpermitForm')->middleware("auth.roles:4,2");
    Route::post('add/spermit', 'Dash\AddController@addSpermit')->name('addSpermit')->middleware("auth.roles:4,2");
    Route::get('add/tspermit', 'Dash\AddController@addTspermitForm')->name('addTspermitForm')->middleware("auth.roles:5,2");
    Route::post('add/tspermit', 'Dash\AddController@addTspermit')->name('addTspermit')->middleware("auth.roles:5,2");
    Route::get('add/summon', 'Dash\AddController@addSummonForm')->name('addSummonForm')->middleware("auth.roles:6,2");
    Route::post('add/summon', 'Dash\AddController@addSummon')->name('addSummon')->middleware("auth.roles:6,2");    

    Route::post('/add/getOwnerPrates', 'Dash\AddController@getOwnerPrates')->name('getOwnerPrates');
    Route::post('/add/getOwnerPratesPost', 'Dash\AddController@getOwnerPratesPost')->name('getOwnerPratesPost');
    Route::post('/add/getPrates', 'Dash\AddController@getPrates')->name('getPrates');
    Route::post('/add/getOwnerSummons', 'Dash\AddController@getOwnerSummons')->name('getOwnerSummons');
    Route::post('/add/getSummons', 'Dash\AddController@getSummons')->name('getSummons');

    Route::get('/view/prate/', 'Dash\ViewController@viewPrates')->name('viewPrates')->middleware("auth.roles:1,3");
    Route::get('/view/prate/{prid}', 'Dash\ViewController@viewPrateDetails')->name('viewPrateDetails')->middleware("auth.roles:1,3");
    Route::get('/view/blpermit/', 'Dash\ViewController@viewBlpermits')->name('viewBlpermits')->middleware("auth.roles:2,3");
    Route::get('/view/blpermit/{blpid}', 'Dash\ViewController@viewBlpermitDetails')->name('viewBlpermitDetails')->middleware("auth.roles:2,3");
    Route::get('/view/bupermit/', 'Dash\ViewController@viewBupermits')->name('viewBupermits')->middleware("auth.roles:3,3");
    Route::get('/view/bupermit/{bupid}', 'Dash\ViewController@viewBupermitDetails')->name('viewBupermitDetails')->middleware("auth.roles:3,3");
    Route::get('/view/spermit/', 'Dash\ViewController@viewSpermits')->name('viewSpermits')->middleware("auth.roles:4,3");
    Route::get('/view/spermit/{spid}', 'Dash\ViewController@viewSpermitDetails')->name('viewSpermitDetails')->middleware("auth.roles:4,3");
    Route::get('/view/tspermit/', 'Dash\ViewController@viewTspermits')->name('viewTspermits')->middleware("auth.roles:5,3");
    Route::get('/view/tspermit/{tspid}', 'Dash\ViewController@viewTspermitDetails')->name('viewTspermitDetails')->middleware("auth.roles:5,3");
    Route::get('/view/summon/', 'Dash\ViewController@viewSummons')->name('viewSummons')->middleware("auth.roles:6,3");
    Route::get('/view/summon/{suid}', 'Dash\ViewController@viewSummonDetails')->name('viewSummonDetails')->middleware("auth.roles:6,3");

    Route::get('/manage/view/', 'Dash\ManageController@viewUsers')->name('viewUsers')->middleware("auth.roles:0,0");
    Route::get('/manage/view/{userid}', 'Dash\ManageController@viewUserDetails')->name('viewUserDetails')->middleware("auth.roles:0,0");
    Route::post('/manage/add/', 'Dash\ManageController@addUser')->name('addUser')->middleware("auth.roles:0,0");
    Route::get('/manage/remove/{userid}', 'Dash\ManageController@removeUser')->name('removeUser')->middleware("auth.roles:0,0");



    Route::post('/add/getOwner', 'Dash\AddController@getOwner')->name('getOwner');
});


