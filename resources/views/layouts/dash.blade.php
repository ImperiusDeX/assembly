<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/dash.js') }}" defer></script>
        @yield("scripts")

        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @yield("css")

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <!-- Our Custom CSS -->

        <!-- Font Awesome JS -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    </head>

    <body id="page-top">

        <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

            <a class="navbar-brand mr-1" href="{{route("home")}}">Assembly</a>

            <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
                <i class="fas fa-bars"></i>
            </button>


            <!-- Navbar Search
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form> -->

            <!-- Navbar -->
            <ul class="navbar-nav d-none d-md-inline-block ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user-circle fa-fw"></i> {{ Auth::user()->name }}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>

        </nav>

        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="sidebar navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="{{route("home")}}">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                @if(auth()->user()->hasAnyRole(1))
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="duePagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-fw fa-chart-area"></i>
                        <span>Due</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="duePagesDropdown">
                        @if(auth()->user()->hasRole(1,1))
                        <a class="dropdown-item" href="{{route('duePrates')}}">Property Rate</a>
                        @endif
                        @if(auth()->user()->hasRole(2,1))
                        <a class="dropdown-item" href="{{route('dueBlpermits')}}">Building Permit</a>
                        @endif
                        @if(auth()->user()->hasRole(3,1))
                        <a class="dropdown-item" href="{{route('dueBupermits')}}">Business Permit</a>
                        @endif
                        @if(auth()->user()->hasRole(4,1))
                        <a class="dropdown-item" href="{{route('dueTspermits')}}">Temporary Structure</a>
                        @endif
                        @if(auth()->user()->hasRole(5,1))
                        <a class="dropdown-item" href="{{route('dueSpermits')}}">Signage & Adverts</a>
                        @endif                   
                    </div>
                </li>
                @endif

                @if(auth()->user()->hasAnyRole(2))
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="addPagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-fw fa-plus-square"></i>
                        <span>Add</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="addPagesDropdown">
                        @if(auth()->user()->hasRole(1,2))
                        <a class="dropdown-item" href="{{route('addPrateForm')}}">Property Rate</a>
                        @endif
                        @if(auth()->user()->hasRole(2,2))
                        <a class="dropdown-item" href="{{route('addBlpermitForm',[])}}">Building Permit</a>
                        @endif
                        @if(auth()->user()->hasRole(3,2))
                        <a class="dropdown-item" href="{{route('addBupermitForm')}}">Business Permit</a>
                        @endif
                        @if(auth()->user()->hasRole(4,2))
                        <a class="dropdown-item" href="{{route('addTspermitForm')}}">Temporary Structure</a>
                        @endif
                        @if(auth()->user()->hasRole(5,2))
                        <a class="dropdown-item" href="{{route('addSpermitForm')}}">Signage & Adverts</a>
                        @endif
                        @if(auth()->user()->hasRole(6,2))
                        <a class="dropdown-item" href="{{route('addSummonForm')}}">Summons</a>
                        @endif
                    </div>
                </li>
                @endif

                @if(auth()->user()->hasAnyRole(3))
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="viewPagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-fw fa-table"></i>
                        <span>View</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="viewPagesDropdown">
                        @if(auth()->user()->hasRole(1,3))
                        <a class="dropdown-item" href="{{route('viewPrates')}}">Property Rate</a>
                        @endif
                        @if(auth()->user()->hasRole(2,3))
                        <a class="dropdown-item" href="{{route('viewBlpermits')}}">Building Permit</a>
                        @endif
                        @if(auth()->user()->hasRole(3,3))
                        <a class="dropdown-item" href="{{route('viewBupermits')}}">Business Permit</a>
                        @endif
                        @if(auth()->user()->hasRole(4,3))
                        <a class="dropdown-item" href="{{route('viewTspermits')}}">Temporary Structure</a>
                        @endif
                        @if(auth()->user()->hasRole(5,3))
                        <a class="dropdown-item" href="{{route('viewSpermits')}}">Signage & Adverts</a>
                        @endif
                        @if(auth()->user()->hasRole(6,3))
                        <a class="dropdown-item" href="{{route('viewSummons')}}">Summons</a>
                        @endif
                    </div>
                </li>
                @endif

                @if(auth()->user()->hasRole(0,0))
                <li class="nav-item">
                    <a class="nav-link" href="{{route('viewUsers')}}">
                        <i class="fas fa-fw fa-user-circle"></i>
                        <span>Manage</span></a>
                </li>
                @endif
            </ul>

            <div id="content-wrapper">

                <div class="container-fluid">
                    @yield('content')

                </div>
                <!-- /.container-fluid -->

                <!-- Sticky Footer -->
                <footer class="sticky-footer">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">

                        </div>
                    </div>
                </footer>

            </div>
            <!-- /.content-wrapper -->

        </div>

    </body>

</html>
