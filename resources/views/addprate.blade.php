@extends('layouts.dash')
@section("css")
<link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/forms.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Property Rate</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('addPrate')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="container">
                            <div class="form-row">
                                <div class="col">
                                    @if($errors->any())
                                    <div class="alert alert-danger" role="alert"><span><strong>{{$errors->first()}}</strong></span></div>
                                    @endif
                                    @if(Session::get("server_error"))
                                    <div class="alert alert-danger" role="alert"><span><strong>{{Session::get("server_error")}}</strong></span></div>
                                    @endif
                                    <h5>Owner Details</h5>
                                    <hr>
                                    <div>
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab-1">New Owner</a></li>
                                            <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-2">Existing Owner</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" role="tabpanel" id="tab-1">
                                                <div class="form-row">
                                                    <div class="col">
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <div class="form-group"><label>Surname</label><input class="form-control" type="text" name="surname" value="{{old("surname")}}"></div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group"><label>First Name</label><input class="form-control" type="text" name="first_name" value="{{old("first_name")}}"></div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col">
                                                        <div class="form-group"><label>Occupation</label><input class="form-control" type="text" name="occupation" value="{{old("occupation")}}"></div>
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <div class="form-group"><label>Email Address</label><input class="form-control" type="text" name="email" value="{{old("email")}}"></div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group"><label>Phone Number</label><input class="form-control" type="text" name="phone" value="{{old("phone")}}"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <div class="form-group"><label>ID Type</label><select class="form-control" name="id_type"><option value="passport" selected="">Passport</option><option value="drivers">Drivers' License</option><option value="nid">National ID</option></select></div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group"><label>ID Number</label><input class="form-control" type="text" name="id_number" value="{{old("id_number")}}"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <div class="form-group d-flex flex-column"><label>ID Scan</label><input type="file" id="id_scan" name="id_scan" accept="image/jpeg"></div>
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="col">
                                                                <div class="form-details">
                                                                    <h5>Next Of Kin</h5>
                                                                    <hr>
                                                                    <div class="form-row">                                                     
                                                                        <div class="col">
                                                                            <div class="form-group"><label>Full Name</label><input class="form-control" type="text" name="kin_name" value="{{old("kin_name")}}"></div>
                                                                        </div>
                                                                        <div class="col">
                                                                            <div class="form-group"><label>Phone Number</label><input class="form-control" type="text" name="kin_phone" value="{{old("kin_phone")}}"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                                <div class="form-row form-details">
                                                    <div class="col-lg-12">
                                                        <h5>Remarks</h5>
                                                        <hr>
                                                        <div class="col"><label class="col-form-label not-required">Remarks</label><textarea class="form-control" name="remarks" value="{{old("remarks")}}"></textarea></div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane" role="tabpanel" id="tab-2">
                                                <div class="form-row">
                                                    <div class="col">
                                                        <div class="form-row">
                                                            <div class="col-lg-5">
                                                                <div class="form-group"><label>ID Type</label>
                                                                    <select class="form-control" name="eid_type" id="eid_type">
                                                                        <option value="passport" selected="">Passport</option>
                                                                        <option value="drivers">Drivers' License</option>
                                                                        <option value="nid">National ID</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-5">
                                                                <div class="form-group"><label>ID Number</label><input class="form-control" type="text" name="eid_number" id="eid_number"></div>
                                                            </div>
                                                            <div class="col">
                                                                <button class="eowner-buttons btn btn-primary align-items-center align-self-end" type="button" id="efind"><i class="fas fa-fw fa-search"></i>Find</button>
                                                                <button class="eowner-buttons btn btn-primary align-items-center align-self-end" type="button" id="ereset"><i class="fas fa-fw fa-undo"></i>Reset</button>
                                                                <input class="form-control" style="display:none" type="text" name="ownerid" id="ownerid"></div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="form-row eowner-details">
                                                    <div class="col">
                                                        <div id="eowner_details" style="display:none">
                                                            <div class="form-row">
                                                                <div class="col">
                                                                    <div class="form-row">
                                                                        <div class="col">
                                                                            <label class="col-form-label">Full Name</label>
                                                                            <p id="eowner_name"></p>
                                                                        </div>
                                                                        <div class="col">
                                                                            <label class="col-form-label">Occupation</label>
                                                                            <p id="eowner_occupation"></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="col"><label class="col-form-label">Email Address</label>
                                                                            <p id="eowner_email"></p>
                                                                        </div>
                                                                        <div class="col"><label class="col-form-label">Phone Number</label>
                                                                            <p id="eowner_phone"></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="col"><label class="col-form-label not-required">Remarks</label>
                                                                            <p id="eowner_remarks">Paragraph</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <div class="form-group d-flex flex-column align-items-center"><img class="rounded" id="eowner_picture" style="width:300px;height: 150px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="eowner_error" class="alert alert-danger" role="alert" style="display: none" ></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-row form-details">
                                            <div class="col">
                                                <div>
                                                    <h5>Property Rate Details</h5>
                                                    <hr>
                                                    <div class="form-row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group"><label>Location</label><input class="form-control form-control-sm" type="text" name="location" value="{{old("location")}}"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group"><label>Size</label><input class="form-control form-control-sm" type="text" name="size" value="{{old("size")}}"></div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group"><label>GH Post Code</label><input class="form-control form-control-sm" type="text" name="ghpost" value="{{old("ghpost")}}"></div>
                                                        </div>

                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group"><label>Fee</label><input class="form-control" type="text" name="fee" value="{{old("fee")}}"></div>
                                                        </div>
                                                    </div>

                                                    <!--<h5>Payment</h5>
                                                    
                                                    @if(auth()->user()->role=="admin")
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group"><label>Amount Paid</label><input class="form-control-plaintext" readonly="" type="text" name="paid" value=""></div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group"><label>Receipt Number</label><input class="form-control" type="text" name="receipt" value="{{old("receipt")}}"></div>
                                                        </div>                          
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group d-flex flex-column"><label>Receipt</label><input type="file" id="receipt_scan" name="receipt_scan" accept="image/jpeg"></div>
                                                        </div>
                                                    </div>
                                                    @endif  -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row text-center form-button">
                                            <div class="col"><button class="btn btn-primary btn-lg" type="submit">Submit</button></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div id="picModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <img class="rounded img-responsive" id="eowner_picture_modal" style="width: 100%;height:100%;padding:15px;">                                   
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        $('#efind').on('click', function () {
            if ($("#eid_type").val() && $("#eid_number").val()) {

                var type = $("#eid_type").val();
                var number = $("#eid_number").val();

                $.ajaxSetup({headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"}});


                $.post("{{route("getOwner")}}", {eid_number: number, eid_type: type}, function (data, status) {
                    console.log(data);
                    var response = $.parseJSON(data);
                    if (response.status == 80) {
                        var name = response.name;
                        var occupation=response.occupation;
                        var email = response.email;
                        var phone = response.phone;
                        var remarks = response.remarks;
                        var ownerid = response.id;


                        $("#eowner_name").text(name);
                        $("#eowner_occupation").text(occupation);
                        $("#eowner_picture").attr('src', response.picture);
                        $("#eowner_picture_modal").attr('src', response.picture);
                        $("#eowner_email").text(email);
                        $("#eowner_phone").text(phone);
                        $("#eowner_remarks").text(remarks);
                        $("#ownerid").val(ownerid);
                        $("#eowner_details").show();
                        $("#eowner_error").text("");
                        $("#eowner_error").hide();
                    } else {

                        $("#eowner_details").hide();
                        $("#ownerid").val("");
                        $("#eowner_error").text("Owner not found");
                        $("#eowner_error").show();

                    }

                });

            } else {
                $("#eowner_details").hide();
                $("#ownerid").val("");
                $("#eowner_error").text("Enter ID Number and ID type");
                $("#eowner_error").show();
            }
        });

        $('#ereset').on('click', function () {
            $("#eowner_details").hide();
            $("#ownerid").val("");
            $("#eowner_error").text("");
            $("#eowner_error").hide();
            $("#eid_number").val("");
        });

        $("#picture").change(function () {
            readURL(this);
        });

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#picture_preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    });

</script>
@endsection
