@extends('layouts.dash')
@section("css")
<link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/forms.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Building Permit</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('addBlpermit')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="container">


                            @if($errors->any())
                            <div class="alert alert-danger" role="alert"><span><strong>{{$errors->first()}}</strong></span></div>
                            @endif
                            @if(Session::get("server_error"))
                            <div class="alert alert-danger" role="alert"><span><strong>{{Session::get("server_error")}}</strong></span></div>
                            @endif


                            <h5>Owner Details</h5>
                            <hr>
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-row">
                                        <div class="col-lg-5">
                                            <div class="form-group"><label>ID Type</label>
                                                <select class="form-control" name="eid_type" id="eid_type">
                                                    <option value="passport" selected="">Passport</option>
                                                    <option value="drivers">Drivers' License</option>
                                                    <option value="nid">National ID</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="form-group"><label>ID Number</label><input class="form-control" type="text" name="eid_number" id="eid_number"></div>
                                        </div>
                                        <div class="col">
                                            <button class="eowner-buttons btn btn-primary align-items-center align-self-end" type="button" id="efind"><i class="fas fa-fw fa-search"></i>Find</button>
                                            <button class="eowner-buttons btn btn-primary align-items-center align-self-end" type="button" id="ereset"><i class="fas fa-fw fa-undo"></i>Reset</button>
                                            <input class="form-control" style="display:none" type="text" name="ownerid" id="ownerid"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row eowner-details">
                                <div class="col">
                                    <div id="eowner_details" style="display:none">
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="form-row">
                                                    <div class="col">
                                                        <label class="col-form-label">Full Name</label>
                                                        <p id="eowner_name"></p>
                                                    </div>
                                                    <div class="col">
                                                        <label class="col-form-label">Occupation</label>
                                                        <p id="eowner_occupation"></p>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col"><label class="col-form-label">Email Address</label>
                                                        <p id="eowner_email"></p>
                                                    </div>
                                                    <div class="col"><label class="col-form-label">Phone Number</label>
                                                        <p id="eowner_phone"></p>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col"><label class="col-form-label not-required">Remarks</label>
                                                        <p id="eowner_remarks">Paragraph</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group d-flex flex-column align-items-center"><img class="rounded" id="eowner_picture" style="width:300px;height: 150px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="eowner_error" class="alert alert-danger" role="alert" style="display: none" ></div>
                                </div>
                            </div>

                            <div class="form-row form-details">
                                <div class="col">
                                    <h5>Property</h5>
                                    <hr>
                                    <div id="eowner_prates" {{(isset($prate) && !empty($prate)) ? "" : "style=display:none;"}}>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Location</th>
                                                        <th>GH Post Code</th>
                                                        <th>Size</th>
                                                        <th>Fee</th>
                                                        <th>Paid</th>
                                                        <th>District</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="eowner_prates_table">
                                                    @if(isset($prate) && !empty($prate))
                                                    <tr>
                                                        <td><input type=radio name=prid value="{{$prate->prid}}" checked=""></td>
                                                        <td>{{$prate->location}}</td>
                                                        <td>{{$prate->ghpost_code}}</td>
                                                        <td>{{$prate->size}}</td>
                                                        <td>{{$prate->fee}}</td>
                                                        <td>{{$prate->paid}}</td>
                                                        <td>{{$prate->district}}</td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div id="eowner_prates_error" class="text-center" {{(isset($prate) && !empty($prate)) ? "style=display:none;" : ""}}>
                                        <p id="eowner_prates_error_text">No available properties</p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row form-details">
                                <div class="col">
                                    <h5>Summons</h5>
                                    <hr>
                                    <div id="eowner_summons" style="display:none">
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>Type Of Building</th>
                                                                <th>Stage Of Construction</th>
                                                                <th>Penalty</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="eowner_summons_table">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col">
                                                <button class="btn btn-primary align-items-center align-self-end" type="button" id="clear"><i class="fas fa-fw fa-undo"></i>Clear selection</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="eowner_summons_error" class="text-center">
                                        <p id="eowner_summon_error_text">No summons found</p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row form-details">
                                <div class="col">
                                    <div>
                                        <h5>Building Permit Details</h5>
                                        <hr>
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="form-group"><label>Type Of Building</label><input class="form-control" type="text" name="type" id="type" value="{{old("type")}}"></div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-lg-6">
                                                <div class="form-group"><label>Fee</label><input class="form-control" type="text" name="fee" value="{{old("fee")}}" id="fee" inputmode="numeric"></div>
                                            </div>
                                            <div class="col-lg-6 text-right">
                                                <label class="col-form-label not-required">Total</label>
                                                <b><p id="total">GH&#x20B5 0.00</p></b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="form-row form-details">
                                <div class="col">
                                    <h5>Payment</h5>
                                    <hr>

                                    <div class="form-row">
                                        <div class="col">
                                            <div class="form-group"><label>Amount Paid</label><input class="form-control-plaintext" readonly="" type="text" name="paid" value="" inputmode="numeric"></div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group"><label>Receipt Number</label><input class="form-control" type="text" name="receipt" value="{{old("receipt")}}"></div>
                                        </div> 
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="form-group d-flex flex-column"><label>Receipt</label><input type="file" id="receipt_scan" name="receipt_scan" accept="image/jpeg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <div class="form-row text-center form-button">
                                <div class="col"><button class="btn btn-primary btn-lg" type="submit">Submit</button></div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#efind').on('click', function () {
            if ($("#eid_type").val() && $("#eid_number").val()) {
                var type = $("#eid_type").val();
                var number = $("#eid_number").val();
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"}});
                $.post("{{route('getOwnerPrates')}}", {eid_number: number, eid_type: type, type: "building"}, function (data, status) {
                    console.log(data);
                    var response = $.parseJSON(data);
                    if (response.status == 80) {
                        var ownerid=response.owner.id;
                        var name = response.owner.name;
                        var occupation = response.owner.occupation;
                        var email = response.owner.email;
                        var phone = response.owner.phone;
                        var remarks = response.owner.remarks;
                        $("#ownerid").val(ownerid);
                        $("#eowner_picture").attr('src', response.owner.picture);
                        $("#eowner_occupation").text(occupation);
                        $("#eowner_name").text(name);
                        $("#eowner_email").text(email);
                        $("#eowner_phone").text(phone);
                        $("#eowner_remarks").text(remarks);
                        $("#eowner_details").show();
                        $("#eowner_prates_table").html("");
                        if (!$.isEmptyObject(response.prates)) {
                            for (let prate in response.prates) {
                                
                                $("#eowner_prates_table").append("<tr>" +
                                        "<td>" + "<input type=radio name=prid value='" + response.prates[prate].prid + "' "+(prate==0 ? "checked=''":"")+">" + "</td>" +
                                        "<td>" + response.prates[prate].location + "</td>" +
                                        "<td>" + response.prates[prate].ghpost + "</td>" +
                                        "<td>" + response.prates[prate].size + "</td>" +
                                        "<td>" + response.prates[prate].fee + "</td>" +
                                        "<td>" + response.prates[prate].paid + "</td>" +
                                        "<td>" + response.prates[prate].district + "</td>" +
                                        "</tr>");
                            }

                            $("#eowner_prates").show();
                            $("#eowner_prates_error").hide();
                        } else {
                            $("#eowner_prates").hide();
                            $("#eowner_prates_error").show();
                        }


                        $("#eowner_summons_table").html("");
                        if (!$.isEmptyObject(response.summons)) {
                            for (let summon in response.summons) {
                                $("#eowner_summons_table").append("<tr>" +
                                        "<td>" + "<input type=radio name=suid value='" + response.summons[summon].suid + "' id='" + response.summons[summon].penalty + "'>" + "</td>" +
                                        "<td>" + response.summons[summon].type + "</td>" +
                                        "<td>" + response.summons[summon].stage + "</td>" +
                                        "<td>" + response.summons[summon].penalty + "</td>" +
                                        "</tr>");
                            }

                            $("#eowner_summons").show();
                            $("#eowner_summons_error").hide();
                        } else {
                            $("#eowner_summons").hide();
                            $("#eowner_summons_error").show();
                        }

                        $("#eowner_error").text("");
                        $("#eowner_error").hide();
                    } else {

                        $("#eowner_details").hide();
                        $("#eowner_error").text("Owner not found");
                        $("#eowner_error").show();
                        $("#eowner_prates").hide();
                        $("#eowner_prates_error").show();
                        $("#eowner_summons").hide();
                        $("#eowner_summons_error").show();
                    }

                });
            } else {
                $("#eowner_details").hide();
                $("#ownerid").val("");
                $("#eowner_error").text("Enter ID Number and ID type");
                $("#eowner_error").show();
            }
        });
        $('#ereset').on('click', function () {
            $("#eowner_details").hide();
            $("#ownerid").val("");
            $("#eowner_error").text("");
            $("#eowner_error").hide();
            $("#eid_number").val("");
            $("#eowner_prates_table").html("");
            $("#eowner_prates").hide();
            $("#eowner_prates_error").show();
            $("#eowner_summons_table").html("");
            $("#eowner_summons").hide();
            $("#eowner_summons_error").show();
        });


        $('#clear').on('click', function () {
            $("[name=suid]").prop("checked", false);
            var fee = $("#fee").val();
            var total = Number(fee);
            $("#total").html("GH&#x20B5 " + String(total));
        });
        
        
        $("#eowner_summons_table").on("change", "[name=suid]", function () {
            var penalty = $(this).attr("id");
            var fee = $("#fee").val();
            var total = Number(penalty) + Number(fee);
            $("#total").html("GH&#x20B5 " + String(total));
        });
        
        
        $("#eowner_prates_table").on("change", "[name=prid]", function () {
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"}});
               $.post("{{route('getOwnerSummons')}}", {prid: $(this).val(),ownerid:$("#ownerid").val(),type:"building"}, function (data, status) {
                    console.log(data);
                    var response = $.parseJSON(data);
                    if (response.status == 80) {
                        $("#eowner_summons_table").html("");
                        if (!$.isEmptyObject(response.summons)) {
                            for (let summon in response.summons) {
                                $("#eowner_summons_table").append("<tr>" +
                                        "<td>" + "<input type=radio name=suid value='" + response.summons[summon].suid + "' id='" + response.summons[summon].penalty + "'>" + "</td>" +
                                        "<td>" + response.summons[summon].type + "</td>" +
                                        "<td>" + response.summons[summon].stage + "</td>" +
                                        "<td>" + response.summons[summon].penalty + "</td>" +
                                        "</tr>");
                            }

                            $("#eowner_summons").show();
                            $("#eowner_summons_error").hide();
                        } else {
                            $("#eowner_summons").hide();
                            $("#eowner_summons_error").show();
                        }

                    } else {
                        $("#eowner_summons").hide();
                        $("#eowner_summons_error").show();
                    }

                });
        });
        
        $("#fee").on("change", function () {
            var penalty = $('input[name=suid]:checked').attr("id");
            if (!penalty) {
                penalty = 0;
            }
            var fee = $("#fee").val();
            var total = Number(penalty) + Number(fee);
            $("#total").html("GH&#x20B5 " + String(total));
        });
    });

</script>
@endsection
