@extends('layouts.dash')
@section("css")
<link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/forms.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h1>Hello {{auth()->user()->name}},</h1>
                    <p class="card-text">Welcome to the Assembly Record Management System. See the <a href="#">FAQ</a> section for more information</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
