@extends('layouts.dash')
@section("css")
<link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/forms.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Temporary Structure Permit</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('addTspermit')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="container">
                            <div class="form-row">
                                <div class="col">

                                    @if($errors->any())
                                    <div class="alert alert-danger" role="alert"><span><strong>{{$errors->first()}}</strong></span></div>
                                    @endif
                                    @if(Session::get("server_error"))
                                    <div class="alert alert-danger" role="alert"><span><strong>{{Session::get("server_error")}}</strong></span></div>
                                    @endif
                                    <h5>Owner Details</h5>
                                    <hr>
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab-1">New Owner</a></li>
                                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-2">Existing Owner</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" role="tabpanel" id="tab-1">
                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group"><label>Surname</label><input class="form-control" type="text" name="surname" value="{{old("surname")}}"></div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group"><label>First Name</label><input class="form-control" type="text" name="first_name" value="{{old("first_name")}}"></div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!--div class="col-lg-5">
                                                    <div class="form-group d-flex flex-column align-items-center"><label>Picture</label><img class="rounded" src="{{asset("images/profile.jpg")}}" id="picture_preview" style="width: 150px;height: 150px;padding:15px;"><input type="file" id="picture" name="picture" accept="image/jpeg"></div>
                                                </div -->
                                            </div>
                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-group"><label>Occupation</label><input class="form-control" type="text" name="occupation" value="{{old("occupation")}}"></div>
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group"><label>Email Address</label><input class="form-control" type="text" name="email" value="{{old("email")}}"></div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group"><label>Phone Number</label><input class="form-control" type="text" name="phone" value="{{old("phone")}}"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group"><label>ID Type</label><select class="form-control" name="id_type"><option value="passport" selected="">Passport</option><option value="drivers">Drivers' License</option><option value="nid">National ID</option></select></div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group"><label>ID Number</label><input class="form-control" type="text" name="id_number" value="{{old("id_number")}}"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group d-flex flex-column"><label>ID Scan</label><input type="file" id="id_scan" name="id_scan" accept="image/jpeg"></div>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-details">
                                                                <h5>Next Of Kin</h5>
                                                                <hr>
                                                                <div class="form-row">                                                     
                                                                    <div class="col">
                                                                        <div class="form-group"><label>Full Name</label><input class="form-control" type="text" name="kin_name" value="{{old("kin_name")}}"></div>
                                                                    </div>
                                                                    <div class="col">
                                                                        <div class="form-group"><label>Phone Number</label><input class="form-control" type="text" name="kin_phone" value="{{old("kin_phone")}}"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="form-row form-details">
                                                <div class="col-lg-12">
                                                    <h5>Remarks</h5>
                                                    <hr>
                                                    <div class="col"><label class="col-form-label not-required">Remarks</label><textarea class="form-control" name="remarks" value="{{old("remarks")}}"></textarea></div>
                                                </div>
                                            </div>

                                            <div class="form-row form-details">
                                                <div class="col">
                                                    <h5>Property Details</h5>
                                                    <hr>
                                                    <div class="form-row">
                                                        <div class="col-lg-8">
                                                            <div class="form-group"><label>Type</label>
                                                                <select class="form-control" name="ptype" id="ptype">
                                                                    <option value="rented" selected="">Rented</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <label>GH Post Code</label>
                                                            <div class="form-group input-group">                                 
                                                                <input type="text" id="prates_ghpost" class="form-control" placeholder="Search for property" aria-label="Search" aria-describedby="basic-addon2" value="{{isset($query) ? $query:""}}">
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-primary" type="button" id="prates_find">
                                                                        <i class="fas fa-search"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="form-row" id="prates_content" style="display: none">
                                                        <div class="col">
                                                            <div class="form-row">
                                                                <div class="table-responsive">
                                                                    <table class="table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th></th>
                                                                                <th>Owner Name</th>
                                                                                <th>Location</th>
                                                                                <th>GH Post Code</th>
                                                                                <th>Size</th>
                                                                                <th>District</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="prates_table">

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="form-row">
                                                                <div class="col">
                                                                    <div>
                                                                        <div class="form-group d-flex flex-column"><label>Documentation</label><input type="file" id="property_scan" name="property_scan" accept="image/jpeg"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="prates_error" class="text-center">
                                                        <p>No available properties</p>
                                                    </div>


                                                </div>
                                            </div>

                                        </div>
                                        <div class="tab-pane" role="tabpanel" id="tab-2">
                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-row">
                                                        <div class="col-lg-5">
                                                            <div class="form-group"><label>ID Type</label>
                                                                <select class="form-control" name="eid_type" id="eid_type">
                                                                    <option value="passport" selected="">Passport</option>
                                                                    <option value="drivers">Drivers' License</option>
                                                                    <option value="nid">National ID</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <div class="form-group"><label>ID Number</label><input class="form-control" type="text" name="eid_number" id="eid_number"></div>
                                                        </div>
                                                        <div class="col">
                                                            <button class="eowner-buttons btn btn-primary align-items-center align-self-end" type="button" id="efind"><i class="fas fa-fw fa-search"></i>Find</button>
                                                            <button class="eowner-buttons btn btn-primary align-items-center align-self-end" type="button" id="ereset"><i class="fas fa-fw fa-undo"></i>Reset</button>
                                                            <input class="form-control" style="display:none" type="text" name="ownerid" id="ownerid"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-row eowner-details">
                                                <div class="col">
                                                    <div id="eowner_details" style="display:none">
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <div class="form-row">
                                                                    <div class="col">
                                                                        <label class="col-form-label">Full Name</label>
                                                                        <p id="eowner_name"></p>
                                                                    </div>
                                                                    <div class="col">
                                                                        <label class="col-form-label">Occupation</label>
                                                                        <p id="eowner_occupation"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="form-row">
                                                                    <div class="col"><label class="col-form-label">Email Address</label>
                                                                        <p id="eowner_email"></p>
                                                                    </div>
                                                                    <div class="col"><label class="col-form-label">Phone Number</label>
                                                                        <p id="eowner_phone"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="form-row">
                                                                    <div class="col"><label class="col-form-label not-required">Remarks</label>
                                                                        <p id="eowner_remarks">Paragraph</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <div class="form-group d-flex flex-column align-items-center"><img class="rounded" id="eowner_picture" style="width:300px;height: 150px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="eowner_error" class="alert alert-danger" role="alert" style="display: none" ></div>
                                                </div>
                                            </div>

                                            <div class="form-row form-details">
                                                <div class="col">
                                                    <h5>Property Details</h5>
                                                    <hr>
                                                    <div class="form-row">
                                                        <div class="col-lg-8">
                                                            <div class="form-group"><label>Type</label>
                                                                <select class="form-control" name="eowner_ptype" id="eowner_ptype">
                                                                    <option value="owned" selected="">Owned</option>
                                                                    <option value="rented">Rented</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <label>GH Post Code</label>
                                                            <div class="form-group input-group">                                 
                                                                <input type="text" id="eowner_prates_ghpost" class="form-control" placeholder="Search for property" aria-label="Search" aria-describedby="basic-addon2">
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-primary" type="button" id="eowner_prates_find">
                                                                        <i class="fas fa-search"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="form-row" id="eowner_prates_content" style="display: none">
                                                        <div class="col">
                                                            <div class="form-row">
                                                                <div class="table-responsive">
                                                                    <table class="table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th></th>
                                                                                <th>Owner Name</th>
                                                                                <th>Location</th>
                                                                                <th>GH Post Code</th>
                                                                                <th>Size</th>
                                                                                <th>District</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="eowner_prates_table">

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="form-row" id="eowner_prates_property_scan" style="display: none">
                                                                <div class="col">
                                                                    <div>
                                                                        <div class="form-group d-flex flex-column"><label>Documentation</label><input type="file" id="eowner_property_scan" name="eowner_property_scan" accept="image/jpeg"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="eowner_prates_error" class="text-center">
                                                        <p>No available properties</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row form-details">
                                        <div class="col">
                                            <h5>Summons</h5>
                                            <hr>
                                            <div id="eowner_summons" style="display: none">
                                                <div class="form-row">
                                                    <div class="col">
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>Type Of Building</th>
                                                                        <th>Stage Of Construction</th>
                                                                        <th>Penalty</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="eowner_summons_table">

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-row">

                                                    <div class="col">
                                                        <button class="btn btn-primary align-items-center align-self-end" type="button" id="clear"><i class="fas fa-fw fa-undo"></i>Clear selection</button>
                                                    </div>
                                                </div>

                                            </div>

                                            <div id="eowner_summons_error" class="text-center">
                                                <p id="eowner_summon_error_text">No summons found</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row form-details">
                                        <div class="col">
                                            <div>
                                                <h5>Building Details</h5>
                                                <hr>
                                                <div class="form-row">
                                                    <div class="col">
                                                        <div class="form-group"><label>Type Of Building</label><input class="form-control" type="text" name="type" id="type"></div>
                                                    </div>
                                                </div>
                                                <!-- <div class="form-row">
                                                     <div class="col">
                                                         <div class="form-group"><label>Location</label><input class="form-control" type="text" name="location" value="{{old("location")}}"></div>
                                                     </div>
                                                     <div class="col">
                                                         <div class="form-group"><label>GH Postcode</label><input class="form-control" type="text" name="ghpost" value="{{old("ghpost")}}"></div>
                                                     </div>
                                                 </div> -->
                                                <div class="form-row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group"><label>Fee</label><input class="form-control" type="text" name="fee" value="{{old("fee")}}" id="fee" inputmode="numeric"></div>
                                                    </div>
                                                    <div class="col-lg-6 text-right">
                                                        <label class="col-form-label not-required">Total</label>
                                                        <b><p id="total">GH&#x20B5 0.00</p></b>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <!--  <div class="form-row form-details">
                                          <div class="col">
                                              <h5>Payment</h5>
                                              <hr>
                                              <div class="form-row">
                                                  <div class="col">
                                                      <div class="form-group"><label>Amount Paid</label><input class="form-control-plaintext" readonly="" type="text" name="paid" value="" inputmode="numeric"></div>
                                                  </div>
                                                  <div class="col">
                                                      <div class="form-group"><label>Receipt Number</label><input class="form-control" type="text" name="receipt" value="{{old("receipt")}}"></div>
                                                  </div> 
                                              </div>
                                              <div class="form-row">
                                                  <div class="col">
                                                      <div class="form-group d-flex flex-column"><label>Receipt</label><input type="file" id="receipt_scan" name="receipt_scan" accept="image/jpeg"></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div> -->

                                    <div class="form-row text-center form-button">
                                        <div class="col"><button class="btn btn-primary btn-lg" type="submit">Submit</button></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="picModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <img class="rounded img-responsive" id="eowner_picture_modal" style="width: 100%;height:100%;padding:15px;">                                   
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#efind').on('click', function () {
            if ($("#eid_type").val() && $("#eid_number").val()) {

                var type = $("#eid_type").val();
                var number = $("#eid_number").val();
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"}});
                $.post("{{route('getOwnerPrates')}}", {eid_number: number, eid_type: type, type: "temp"}, function (data, status) {
                    console.log(data);
                    var response = $.parseJSON(data);
                    $("#eowner_summons_table").html("");
                    $("#eowner_prates_table").html(" ");
                    if (response.status == 80) {
                        var ownerid = response.owner.id;
                        var name = response.owner.name;
                        var occupation = response.owner.occupation;
                        var email = response.owner.email;
                        var phone = response.owner.phone;
                        var remarks = response.owner.remarks;
                        $("#eowner_picture").attr('src', response.owner.picture);
                        $("#eowner_picture_modal").attr('src', response.owner.picture);
                        $("#eowner_name").text(name);
                        $("#eowner_occupation").text(occupation);
                        $("#eowner_email").text(email);
                        $("#eowner_phone").text(phone);
                        $("#eowner_details").show();
                        $("#eowner_remarks").text(remarks);
                        $("#ownerid").val(ownerid);
                        $("#eowner_ptype").val("owned");
                        $("#eowner_prates_property_scan").hide();
                        if (!$.isEmptyObject(response.prates)) {
                            for (let prate in response.prates) {
                                $("#eowner_prates_table").append("<tr>" +
                                        "<td>" + "<input type=radio name=prid value='" + response.prates[prate].prid + "' "+(prate==0 ? "checked=''":"")+">" + "</td>" +
                                        "<td>" + name + "</td>" +
                                        "<td>" + response.prates[prate].location + "</td>" +
                                        "<td>" + response.prates[prate].ghpost + "</td>" +
                                        "<td>" + response.prates[prate].size + "</td>" +
                                        "<td>" + response.prates[prate].district + "</td>" +
                                        "</tr>");
                            }

                            $("#eowner_prates_content").show();
                            $("#eowner_prates_error").hide();
                        } else {
                            $("#eowner_prates_content").hide();
                            $("#eowner_prates_error").show();
                        }

                        if (!$.isEmptyObject(response.summons)) {
                            for (let summon in response.summons) {
                                $("#eowner_summons_table").append("<tr>" +
                                        "<td>" + "<input type=radio name=suid value='" + response.summons[summon].suid + "' id='" + response.summons[summon].penalty + "'>" + "</td>" +                                  
                                        "<td>" + response.summons[summon].type + "</td>" +
                                        "<td>" + response.summons[summon].stage + "</td>" +
                                        "<td>" + response.summons[summon].penalty + "</td>" +
                                        "</tr>");
                            }

                            $("#eowner_summons").show();
                            $("#eowner_summons_error").hide();
                        } else {
                            $("#eowner_summons").hide();
                            $("#eowner_summons_error").show();
                        }

                        $("#eowner_error").text("");
                        $("#eowner_error").hide();
                    } else {
                        $("#ownerid").val("");
                        $("#eowner_details").hide();
                        $("#eowner_error").text("Owner not found");
                        $("#eowner_error").show();
                        $("#eowner_summons").hide();
                        $("#eowner_summons_error").show();
                    }

                });
            } else {
                $("#eowner_details").hide();
                $("#ownerid").val("");
                $("#eowner_error").text("Enter ID Number and ID type");
                $("#eowner_error").show();
            }
        });
        $('#ereset').on('click', function () {
            $("#eowner_details").hide();
            $("#ownerid").val("");
            $("#eowner_error_text").text("");
            $("#eowner_error").hide();
            $("#eid_number").val("");
            $("#eowner_summons_table").html("");
            $("#eowner_summons").hide();
            $("#eowner_summons_error").show();
        });
        
        $("#picture").change(function () {
            readURL(this);
        });
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#picture_preview').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#prates_find').on('click', function () {
            if ($("#prates_ghpost").val()) {

                var code = $("#prates_ghpost").val();
                var ptype = $("#ptype").val();
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"}});
                $.post("{{route('getPrates')}}", {prates_ghpost: code, ptype: ptype,type:"temp"}, function (data, status) {
                    console.log(data);
                    var response = $.parseJSON(data);
                    $("#prates_table").html("");
                    $("#eowner_summons_table").html("");
                    if (response.status == 80) {
                        if (!$.isEmptyObject(response.prates)) {
                            for (let prate in response.prates) {
                                $("#prates_table").append("<tr>" +
                                        "<td>" + "<input type=radio name=prid value='" + response.prates[prate].prid + "' "+(prate==0 ? "checked=''":"")+">" + "</td>" +
                                        "<td>" + response.prates[prate].name + "</td>" +
                                        "<td>" + response.prates[prate].location + "</td>" +
                                        "<td>" + response.prates[prate].ghpost + "</td>" +
                                        "<td>" + response.prates[prate].size + "</td>" +
                                        "<td>" + response.prates[prate].district + "</td>" +
                                        "</tr>");
                            }
                        }
                        
                        if (!$.isEmptyObject(response.summons)) {
                            for (let summon in response.summons) {
                                $("#eowner_summons_table").append("<tr>" +
                                        "<td>" + "<input type=radio name=suid value='" + response.summons[summon].suid + "' id='" + response.summons[summon].penalty + "'>" + "</td>" +
                                        "<td>" + response.summons[summon].type + "</td>" +
                                        "<td>" + response.summons[summon].stage + "</td>" +
                                        "<td>" + response.summons[summon].penalty + "</td>" +
                                        "</tr>");
                            }

                            $("#eowner_summons").show();
                            $("#eowner_summons_error").hide();
                        } else {
                            $("#eowner_summons").hide();
                            $("#eowner_summons_error").show();
                        }


                        $("#prates_content").show();
                        $("#prates_error").hide();
                    } else {
                        $("#prates_content").hide();
                        $("#prates_error").show();
                    }
                });
            }
        });
        $('#eowner_prates_find').on('click', function () {
            loadOwnerPrates();
        });
        $("#eowner_ptype").change(function () {
            $("#eowner_prates_content").hide();
            $("#eowner_prates_table").html("");
            $("#eowner_prates_ghpost").val("");
            $("#eowner_prates_error").show();
            if ($(this).val() == "owned") {
                loadOwnerPrates();
                $("#eowner_prates_property_scan").hide();
            } else {
                $("#eowner_prates_property_scan").show();
            }
        });
        function loadOwnerPrates() {
            if ($("#ownerid").val()) {

                var code = $("#eowner_prates_ghpost").val();
                var ownerid = $("#ownerid").val();
                var ptype = $("#eowner_ptype").val();
                if (ptype == "rented" && !code) {
                    return;
                }

                $.ajaxSetup({headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"}});
                $.post("{{route('getPrates')}}", {prates_ghpost: code, ownerid: ownerid, ptype: ptype,type:"temp"}, function (data, status) {
                    console.log(data);
                    var response = $.parseJSON(data);
                    $("#eowner_prates_table").html("");
                    $("#eowner_summons_table").html("");
                    if (response.status == 80) {
                        if (!$.isEmptyObject(response.prates)) {
                            for (let prate in response.prates) {
                                $("#eowner_prates_table").append("<tr>" +
                                        "<td>" + "<input type=radio name=prid value='" + response.prates[prate].prid + "' "+(prate==0 ? "checked=''":"")+">" + "</td>" +
                                        "<td>" + response.prates[prate].name + "</td>" +
                                        "<td>" + response.prates[prate].location + "</td>" +
                                        "<td>" + response.prates[prate].ghpost + "</td>" +
                                        "<td>" + response.prates[prate].size + "</td>" +
                                        "<td>" + response.prates[prate].district + "</td>" +
                                        "</tr>");
                            }
                        }
                        
                        if (!$.isEmptyObject(response.summons)) {
                            for (let summon in response.summons) {
                                $("#eowner_summons_table").append("<tr>" +
                                        "<td>" + "<input type=radio name=suid value='" + response.summons[summon].suid + "' id='" + response.summons[summon].penalty + "'>" + "</td>" +
                                        "<td>" + response.summons[summon].type + "</td>" +
                                        "<td>" + response.summons[summon].stage + "</td>" +
                                        "<td>" + response.summons[summon].penalty + "</td>" +
                                        "</tr>");
                            }

                            $("#eowner_summons").show();
                            $("#eowner_summons_error").hide();
                        } else {
                            $("#eowner_summons").hide();
                            $("#eowner_summons_error").show();
                        }

                        $("#eowner_prates_content").show();
                        $("#eowner_prates_error").hide();
                    } else {
                        $("#eowner_prates_content").hide();
                        $("#eowner_prates_error").show();
                    }
                });
            }
        }
        
        
        function loadOwnerSummons(prid){
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"}});
            $.post("{{route('getOwnerSummons')}}", {prid:prid, ownerid: $("#ownerid").val(), type: "signage"}, function (data, status) {
                console.log(data);
                var response = $.parseJSON(data);
                if (response.status == 80) {
                    $("#eowner_summons_table").html("");
                    if (!$.isEmptyObject(response.summons)) {
                        for (let summon in response.summons) {
                            $("#eowner_summons_table").append("<tr>" +
                                    "<td>" + "<input type=radio name=suid value='" + response.summons[summon].suid + "' id='" + response.summons[summon].penalty + "'>" + "</td>" +
                                    "<td>" + response.summons[summon].type + "</td>" +
                                    "<td>" + response.summons[summon].stage + "</td>" +
                                    "<td>" + response.summons[summon].penalty + "</td>" +
                                    "</tr>");
                        }

                        $("#eowner_summons").show();
                        $("#eowner_summons_error").hide();
                    } else {
                        $("#eowner_summons").hide();
                        $("#eowner_summons_error").show();
                    }

                } else {
                    $("#eowner_summons").hide();
                    $("#eowner_summons_error").show();
                }

            });
        }

        $('#clear').on('click', function () {
            $("[name=suid]").prop("checked", false);
            var fee = $("#fee").val();
            var total = Number(fee);
            $("#total").html("GH&#x20B5 " + String(total));
        });
        $("#eowner_summons_table").on("change", "[name=suid]", function () {
            var penalty = $(this).attr("id");
            var fee = $("#fee").val();
            var total = Number(penalty) + Number(fee);
            $("#total").html("GH&#x20B5 " + String(total));
        });
        
        
        $("#eowner_prates_table").on("change", "[name=prid]", function () {
            loadOwnerSummons($(this).val());
        });
        
        $("#prates_table").on("change", "[name=prid]", function () {
            loadOwnerSummons($(this).val());
        });


        $("#fee").on("change", function () {
            var penalty = $('input[name=suid]:checked').attr("id");
            if (!penalty) {
                penalty = 0;
            }
            var fee = $("#fee").val();
            var total = Number(penalty) + Number(fee);
            $("#total").html("GH&#x20B5 " + String(total));
        });
    });
</script>
@endsection
