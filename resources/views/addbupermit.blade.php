@extends('layouts.dash')
@section("css")
<link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/forms.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Business Permit</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('addBupermit')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="container">
                            @if($errors->any())
                            <div class="alert alert-danger" role="alert"><span><strong>{{$errors->first()}}</strong></span></div>
                            @endif
                            @if(Session::get("server_error"))
                            <div class="alert alert-danger" role="alert"><span><strong>{{Session::get("server_error")}}</strong></span></div>
                            @endif

                            <h5>Owner Details</h5>
                            <hr>
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-row">
                                        <div class="col-lg-5">
                                            <div class="form-group"><label>ID Type</label>
                                                <select class="form-control" name="eid_type" id="eid_type">
                                                    <option value="passport" selected="">Passport</option>
                                                    <option value="drivers">Drivers' License</option>
                                                    <option value="nid">National ID</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="form-group"><label>ID Number</label><input class="form-control" type="text" name="eid_number" id="eid_number"></div>
                                        </div>
                                        <div class="col">
                                            <button class="eowner-buttons btn btn-primary align-items-center align-self-end" type="button" id="efind"><i class="fas fa-fw fa-search"></i>Find</button>
                                            <button class="eowner-buttons btn btn-primary align-items-center align-self-end" type="button" id="ereset"><i class="fas fa-fw fa-undo"></i>Reset</button>
                                            <input class="form-control" style="display:none" type="text" name="ownerid" id="ownerid"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row eowner-details">
                                <div class="col">
                                    <div id="eowner_details" style="display:none">
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="form-row">
                                                    <div class="col">
                                                        <label class="col-form-label">Full Name</label>
                                                        <p id="eowner_name"></p>
                                                    </div>
                                                    <div class="col">
                                                        <label class="col-form-label">Occupation</label>
                                                        <p id="eowner_occupation"></p>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col"><label class="col-form-label">Email Address</label>
                                                        <p id="eowner_email"></p>
                                                    </div>
                                                    <div class="col"><label class="col-form-label">Phone Number</label>
                                                        <p id="eowner_phone"></p>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col"><label class="col-form-label not-required">Remarks</label>
                                                        <p id="eowner_remarks">Paragraph</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group d-flex flex-column align-items-center"><img class="rounded" id="eowner_picture" style="width:300px;height: 150px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="eowner_error" class="alert alert-danger" role="alert" style="display: none" ></div>
                                </div>
                            </div>

                            <div class="form-row form-details">
                                <div class="col">
                                    <h5>Property</h5>
                                    <hr>
                                    <div id="eowner_prates" {{(isset($prate) && !empty($prate)) ? "" : "style=display:none;"}}>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Location</th>
                                                        <th>Size</th>
                                                        <th>Fee</th>
                                                        <th>Paid</th>
                                                        <th>District</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="eowner_prates_table">
                                                    @if(isset($prate) && !empty($prate))
                                                    <tr>
                                                        <td><input type=radio name=prid value="{{$prate->prid}}" checked=""></td>
                                                        <td>{{$prate->location}}</td>
                                                        <td>{{$prate->size}}</td>
                                                        <td>{{$prate->fee}}</td>
                                                        <td>{{$prate->paid}}</td>
                                                        <td>{{$prate->district}}</td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div id="eowner_prates_error" class="text-center" {{(isset($prate) && !empty($prate)) ? "style=display:none;" : ""}}>
                                        <p id="eowner_prates_error_text">No available properties</p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row form-details">
                                <div class="col">
                                    <div>
                                        <h5>Business Permit Details</h5>
                                        <hr>
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="form-group" id="names"><label>Business Name</label>
                                                    @if(!empty(old("name")))
                                                    @foreach(old("name") as $name)
                                                    <input class="form-control" style="margin-bottom: 5px;" type="text" name="name[]" placeholder="Enter business name" value="{{$name}}">
                                                    @endforeach
                                                    @else
                                                    <input class="form-control" style="margin-bottom: 5px;" type="text" name="name[]" placeholder="Enter business name">
                                                    @endif
                                                </div>
                                                <button class="btn btn-primary align-items-center align-self-end" type="button" id="add"><i class="fas fa-fw fa-plus-circle"></i>Add</button>
                                            </div>
                                        </div>
                                        <div class="form-row" style="margin-top: 10px">
                                            <div class="col-lg-12">
                                                <div class="form-group"><label>Fee</label><input class="form-control" type="text" name="fee" value="{{old("fee")}}" id="fee" inputmode="numeric"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--<div class="form-row form-details">
                                <div class="col">
                                    <h5>Payment</h5>
                                    <hr>
                                    <div class="form-row">
                                        <div class="col-lg-6">
                                            <div class="form-group"><label>Amount Paid</label><input class="form-control-plaintext" readonly="" type="text" name="paid" value="" inputmode="numeric"></div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group"><label>Receipt Number</label><input class="form-control" type="text" name="receipt" value="{{old("receipt")}}"></div>
                                        </div> 
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="form-group d-flex flex-column"><label>Receipt</label><input type="file" id="receipt_scan" name="receipt_scan" accept="image/jpeg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->


                            <div class="form-row text-center form-button">
                                <div class="col"><button class="btn btn-primary btn-lg" type="submit">Submit</button></div>
                            </div>


                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#efind').on('click', function () {
            if ($("#eid_type").val() && $("#eid_number").val()) {

                var type = $("#eid_type").val();
                var number = $("#eid_number").val();
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"}});
                $.post("{{route('getOwnerPrates')}}", {eid_number: number, eid_type: type}, function (data, status) {
                    console.log(data);
                    var response = $.parseJSON(data);
                    if (response.status == 80) {
                        var name = response.owner.name;
                        var occupation = response.owner.occupation;
                        var email = response.owner.email;
                        var phone = response.owner.phone;
                        var remarks = response.owner.remarks;
                        $("#eowner_picture").attr('src', response.owner.picture);
                        $("#eowner_name").text(name);
                        $("#eowner_occupation").text(occupation);
                        $("#eowner_email").text(email);
                        $("#eowner_phone").text(phone);
                        $("#eowner_details").show();
                        $("#eowner_remarks").text(remarks);
                        $("#eowner_prates_table").html("");
                        if (!$.isEmptyObject(response.prates)) {
                            for (let prate in response.prates) {
                                $("#eowner_prates_table").append("<tr>" +
                                        "<td>" + "<input type=radio name=prid value='" + response.prates[prate].prid + "' " + (prate == 0 ? "checked=''" : "") + ">" + "</td>" +
                                        "<td>" + response.prates[prate].location + "</td>" +
                                        "<td>" + response.prates[prate].size + "</td>" +
                                        "<td>" + response.prates[prate].fee + "</td>" +
                                        "<td>" + response.prates[prate].paid + "</td>" +
                                        "<td>" + response.prates[prate].district + "</td>" +
                                        "</tr>");
                            }

                            $("#eowner_prates").show();
                            $("#eowner_prates_error").hide();
                        } else {
                            $("#eowner_prates").hide();
                            $("#eowner_prates_error").show();
                        }


                        $("#eowner_error").text("");
                        $("#eowner_error").hide();
                    } else {

                        $("#eowner_details").hide();
                        $("#eowner_error").text("Owner not found");
                        $("#eowner_error").show();
                        $("#eowner_prates").hide();
                        $("#eowner_prates_error").show();
                    }

                });
            } else {
                $("#eowner_details").hide();
                $("#ownerid").val("");
                $("#eowner_error").text("Enter ID Number and ID type");
                $("#eowner_error").show();
            }
        });
        
        $('#ereset').on('click', function () {
            $("#eowner_details").hide();
            $("#ownerid").val("");
            $("#eowner_error").text("");
            $("#eowner_error").hide();
            $("#eid_number").val("");
            $("#eowner_prates_table").html("");
            $("#eowner_prates").hide();
            $("#eowner_prates_error").show();
        });
        
        $("#add").on("click", function () {
            $("#names").append('<input class="form-control" style="margin-bottom: 5px;" type="text" name="name[]" placeholder="Enter business name">');
        });

    });

</script>
@endsection
