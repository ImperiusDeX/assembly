@extends('layouts.dash')
@section("css")
<link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/forms.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Property Rate</h5>
                </div>
                <div class="card-body">
                    <form>
                        @csrf
                        <div class="container">
                            <div class="form-row">
                                <div class="col">
                                    @if($errors->any())
                                    <div class="alert alert-danger" role="alert"><span><strong>{{$errors->first()}}</strong></span></div>
                                    @endif
                                    @if(Session::get("server_error"))
                                    <div class="alert alert-danger" role="alert"><span><strong>{{Session::get("server_error")}}</strong></span></div>
                                    @endif
                                    <h5>Owner Details</h5>
                                    <hr>
                                    <div>

                                        <div class="form-row">
                                            <div class="col-lg-4 order-lg-last order-md-first">                
                                                <div class="form-group d-flex flex-column align-items-center"><label>Picture</label><a href="#picModal" data-toggle="modal"><img class="rounded" src="{{asset("storage/profiles/" . $owner->ownerid . ".jpg")}}" id="picture_preview" style="width: 300px;height: 150px;padding:15px;"></a></div>                                     
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="form-row">
                                                    <div class="col">
                                                        <div class="form-group"><label>Surname</label><input readonly="" class="form-control-plaintext" type="text" name="surname" value="{{explode(" ",$owner->name)[0]}}"></div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group"><label>First Name</label><input readonly="" class="form-control-plaintext" type="text" name="first_name" value="{{implode(" ", array_slice(explode(" ",$owner->name),1))}}"></div>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="col">
                                                        <div class="form-group"><label>Occupation</label><input readonly="" class="form-control-plaintext" type="text" name="occupation" value="{{$owner->occupation}}"></div>
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <div class="form-group"><label>Email Address</label><input readonly="" class="form-control-plaintext" type="text" name="email" value="{{$owner->email}}"></div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group"><label>Phone Number</label><input readonly="" class="form-control-plaintext" type="text" name="phone" value="{{$owner->phone}}"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label>ID Type</label><input readonly="" class="form-control-plaintext" name="id_type" type="text" value="{{($owner->id_type=="passport") ? "Passport":""}}{{($owner->id_type=="drivers") ? "Drivers' License":""}}{{($owner->id_type=="nid") ? "National ID":""}}">
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group"><label>ID Number</label><input readonly="" class="form-control-plaintext" type="text" name="id_number" value="{{$owner->id_number}}"></div>
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="col">
                                                                <div class="form-details">
                                                                    <h5>Next Of Kin</h5>
                                                                    <hr>
                                                                    <div class="form-row">                                                     
                                                                        <div class="col">
                                                                            <div class="form-group"><label>Full Name</label><input readonly="" class="form-control-plaintext" type="text" name="kin_name" value="{{$owner->kin_name}}"></div>
                                                                        </div>
                                                                        <div class="col">
                                                                            <div class="form-group"><label>Phone Number</label><input readonly="" class="form-control-plaintext" type="text" name="kin_phone" value="{{$owner->kin_phone}}"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row form-details">
                                                    <div class="col">
                                                        <h5>Remarks</h5>
                                                        <hr>
                                                        <div class="col"><label class="col-form-label">Remarks</label><textarea class="form-control-plaintext" name="remarks" value="{{json_decode($owner->extra)->remarks}}"></textarea></div>
                                                    </div>
                                                </div>
                                            </div> 

                                        </div>

                                        <div class="form-row" id="details">
                                            <div class="col">
                                                <div>
                                                    <h5>Property Rate Details</h5>
                                                    <hr>
                                                    <div class="form-row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group"><label>Location</label><input readonly="" class="form-control-plaintext" type="text" name="location" value="{{$prate->location}}"></div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group"><label>Size</label><input readonly="" class="form-control-plaintext" type="text" name="size" value="{{$prate->size}}"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group"><label>GH Post Code</label><input readonly="" class="form-control-plaintext" type="text"  value="{{$prate->ghpost_code}}"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row form-details" id="payment">
                                            <div class="col">
                                                <h5>Payments</h5>
                                                <hr>
                                                <div class="form-row">
                                                    <div class="col">
                                                        <div class="form-group"><label>Fee</label><input readonly="" class="form-control-plaintext" type="text" name="fee" value="{{$prate->total}}"></div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group"><label>Paid</label><input readonly="" class="form-control-plaintext" type="text" value="{{$prate->paid}}"></div>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="col">
                                                        <b><p>Receipts</p></b>
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <div id="eowner_receipts" {{isset($receipts) && !$receipts->isEmpty() ? "" : "style=display:none;"}}>
                                                                    <div class="form-row">
                                                                        <div class="col">
                                                                            <div class="table-responsive">
                                                                                <table class="table">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Receipt No.</th>
                                                                                            <th>Amount</th>
                                                                                            <th>Paid At</th>
                                                                                            <th></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody id="eowner_receipts_table">
                                                                                    @if(isset($receipts) && !$receipts->isEmpty())
                                                                                    @foreach($receipts as $receipt)
                                                                                    <tr>
                                                                                    <td>{{$receipt->receipt_code}}</td>
                                                                                    <td>{{$receipt->paid_amount}}</td>
                                                                                    <td>{{$receipt-> created_at}}</td>
                                                                                    <td><a href="{{asset("storage/receipts/" . $receipt->rid . ".jpg")}}">View Copy</a></td>
                                                                                    </tr>
                                                                                    @endforeach
                                                                                    @endif
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="eowner_summons_error" class="text-center" {{isset($receipts) && !$receipts->isEmpty()  ? "style=display:none;" : ""}}>
                                                                    <p id="eowner_summon_error_text">No receipts</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="picModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <img class="rounded img-responsive" src="{{asset("storage/profiles/" . $owner->ownerid . ".jpg")}}" style="width: 100%;height:100%;padding:15px;">                                   
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
