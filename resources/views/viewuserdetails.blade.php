@extends('layouts.dash')
@section("css")
<link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/forms.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Users</h5>
                </div>
                <div class="card-body">
                    <form>
                        @csrf
                        <div class="container">
                            <div class="form-row">
                                <div class="col">

                                    @if($errors->any())
                                    <div class="alert alert-danger" role="alert"><span><strong>{{$errors->first()}}</strong></span></div>
                                    @endif
                                    @if(Session::get("server_error"))
                                    <div class="alert alert-danger" role="alert"><span><strong>{{Session::get("server_error")}}</strong></span></div>
                                    @endif
                                    <div>
                                        <div class="form-row">
                                            <div class="col">
                                                <h5>User Details</h5>
                                                <hr>
                                                <div class="form-row">
                                                    <div class="col">
                                                        <div class="form-group"><label>Username</label><input readonly="" class="form-control-plaintext" type="text" name="surname" value="{{explode(" ",$user->email)[0]}}"></div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group"><label>Phone Number</label><input readonly="" class="form-control-plaintext" type="text" value="{{$user->phone}}"></div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col">
                                                        <div class="form-group"><label>Surname</label><input readonly="" class="form-control-plaintext" type="text" name="surname" value="{{explode(" ",$user->name)[0]}}"></div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group"><label>First Name</label><input readonly="" class="form-control-plaintext" type="text" name="first_name" value="{{implode(" ", array_slice(explode(" ",$user->name),1))}}"></div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="form-row">                                                  
                                                    <div class="col">
                                                        <div class="form-group"><label>Email</label><input readonly="" class="form-control-plaintext" type="text" value="{{$user->personal_email}}"></div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group"><label>District</label><input readonly="" class="form-control-plaintext" type="text" name="phone" value="{{$user->district}}"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row form-details">
                                            <div class="col">
                                                <h5>Roles</h5>
                                                <hr>
                                                
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>      
                                                                <th>Section</th>
                                                                <th>Due</th>
                                                                <th>Add</th>
                                                                <th>View</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="eowner_summons_table">
                                                            <tr>
                                                                <td>Property Rates</td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(1,1) ? "checked=": ""}}></td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(1,2) ? "checked=": ""}}></td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(1,3) ? "checked=": ""}}></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Building Permit</td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(2,1) ? "checked=": ""}}></td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(2,2) ? "checked=": ""}}></td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(2,3) ? "checked=": ""}}></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Business Permit</td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(3,1) ? "checked=": ""}}></td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(3,2) ? "checked=": ""}}></td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(3,3) ? "checked=": ""}}></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Signage & Adverts</td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(4,1) ? "checked=": ""}}></td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(4,2) ? "checked=": ""}}></td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(4,3) ? "checked=": ""}}></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Temporary Structure</td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(5,1) ? "checked=": ""}}></td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(5,2) ? "checked=": ""}}></td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(5,3) ? "checked=": ""}}></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Summons</td>
                                                                <td></td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(6,2) ? "checked=": ""}}></td>
                                                                <td><input type="checkbox" disabled="" {{ $user->hasRole(6,3) ? "checked=": ""}}></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-row text-center form-button">
                                            <div class="col"><a href="{{route("removeUser",["userid"=>$user->id])}}"><button class="btn btn-danger btn-lg" type="button">Remove User</button></a></div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
