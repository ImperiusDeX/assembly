@extends('layouts.dash')
@section("css")
<link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/forms.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card" style="margin-bottom: 10px">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form action="{{route("viewBlpermits")}}" method="GET">
                                       
                                        <div class="form-group input-group">
                                            <input type="text" name="query" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" value="{{isset($query) ? $query:""}}">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="fas fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                           <!-- <label style="margin-right:5px"><input style="margin-left:5px" type="radio" name="type" value="all" checked="">All</label>
                                            <label style="margin-right:5px"><input style="margin-left:5px" type="radio" name="type" value="created" {{(isset($type) && $type=="created") ? "checked=":""}}>Created</label>
                                            <label style="margin-right:5px"><input style="margin-left:5px" type="radio" name="type" value="updated" {{(isset($type) && $type=="updated") ? "checked=":""}}>Updated</label> -->
                                            <label style="margin-left:20px"><b>From</b><input style="margin-left:5px" type="date" name="from" value="{{isset($from) ? $from :date("Y-m-j")}}"></label>
                                            <label style="margin-left:10px"><b>To</b><input style="margin-left:5px" type="date" name="to" value="{{isset($to) ? $to :date("Y-m-j")}}"></label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h3>Business Permits</h3>
                            <h5>View</h5>
                        </div>
                    </div>

                    @if(!$bupermits->isEmpty())
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Owner Name</th>
                                    <th>Fee</th>
                                    <th>Paid</th>
                                    <th>Remaining</th>
                                    <th>Year</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bupermits as $bupermit) 
                                <tr>
                                    <td>{{$bupermit->name}}</td>
                                    <td>{{$bupermit->total}}</td>
                                    <td>{{$bupermit->paid}}</td>
                                    <td>{{abs($bupermit->total-$bupermit->paid)}}</td>
                                    <td>{{date("Y",strtotime($bupermit->created_at))}}</td>
                                    <td><a href="{{route("viewBupermitDetails",['bupid'=>$bupermit->bupid])."#details"}}"><button class="btn btn-primary" type="button">View</button></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                        @if(isset($query))
                            {{ $bupermits->appends(["query"=>$query,"from"=>$from,"to"=>$to])->links() }}
                        @else
                            {{ $bupermits->links() }}
                        @endif
                    @else
                    <div class="text-center">
                        <p>No records found</p>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
