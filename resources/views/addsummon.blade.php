@extends('layouts.dash')
@section("css")
<link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/forms.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Summons</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('addSummon')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="container">
                            <div class="form-row">
                                <div class="col">

                                    @if($errors->any())
                                    <div class="alert alert-danger" role="alert"><span><strong>{{$errors->first()}}</strong></span></div>
                                    @endif
                                    @if(Session::get("server_error"))
                                    <div class="alert alert-danger" role="alert"><span><strong>{{Session::get("server_error")}}</strong></span></div>
                                    @endif
                                    <div>

                                        <h5>Owner Details</h5>
                                        <hr>
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="form-row">     
                                                    <div class="col-lg-10">
                                                        <div class="form-group"><label>GH Post Code</label><input class="form-control" type="text" name="ghpost_code" id="ghpost_code"></div>
                                                    </div>
                                                    <div class="col">
                                                        <button class="eowner-buttons btn btn-primary align-items-center align-self-end" type="button" id="efind"><i class="fas fa-fw fa-search"></i>Find</button>
                                                        <button class="eowner-buttons btn btn-primary align-items-center align-self-end" type="button" id="ereset"><i class="fas fa-fw fa-undo"></i>Reset</button>
                                                        <input class="form-control" style="display:none" type="text" name="ownerid" id="ownerid"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row eowner-details">
                                            <div class="col">
                                                <div id="eowner_details" style="display:none">
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-row">
                                                                <div class="col">
                                                                    <label class="col-form-label">Full Name</label>
                                                                    <p id="eowner_name"></p>
                                                                </div>
                                                                <div class="col">
                                                                    <label class="col-form-label">Occupation</label>
                                                                    <p id="eowner_occupation"></p>
                                                                </div>
                                                            </div>
                                                            <div class="form-row">
                                                                <div class="col"><label class="col-form-label">Email Address</label>
                                                                    <p id="eowner_email"></p>
                                                                </div>
                                                                <div class="col"><label class="col-form-label">Phone Number</label>
                                                                    <p id="eowner_phone"></p>
                                                                </div>
                                                            </div>
                                                            <div class="form-row">
                                                                <div class="col"><label class="col-form-label">Remarks</label>
                                                                    <p id="eowner_remarks">Paragraph</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="form-group d-flex flex-column align-items-center"><img class="rounded" id="eowner_picture" style="width:300px;height: 150px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="eowner_error" class="alert alert-danger" role="alert" style="display: none" ></div>
                                            </div>
                                        </div>

                                        <div class="form-row form-details">
                                            <div class="col">
                                                <h5>Property</h5>
                                                <hr>
                                                <div id="eowner_prates" style="display:none">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th></th>
                                                                    <th>Location</th>
                                                                    <th>GH Post Code</th>
                                                                    <th>Size</th>
                                                                    <th>District</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="eowner_prates_table">
                                                              
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div id="eowner_prates_error" class="text-center">
                                                    <p id="eowner_prates_error_text">No available properties</p>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="form-row form-details">
                                            <div class="col">
                                                <div>
                                                    <h5>Summon Details</h5>
                                                    <hr>
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group"><label>Type Of Permit</label><select class="form-control" name="ptype" id="ptype">
                                                                    <option value="building">Building Permit</option>
                                                                    <option value="temp">Temporary Structure Permit</option>
                                                                    <option value="signage">Siganage & Adverts Permit</option>
                                                                </select></div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>



                                        <div class="form-row form-details">
                                            <div class="col">
                                                <div>
                                                    <h5>Structure Details</h5>
                                                    <hr>
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group"><label>Type Of Structure</label><input class="form-control" type="text" name="type" id="type"></div>
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group"><label>Stage Of Construction</label><input class="form-control" type="text" name="stage" value="{{old("stage")}}"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row form-details">
                                            <div class="col-lg-12">
                                                <h5>Penalty</h5>
                                                <hr>
                                                <div class="col">
                                                    <div class="form-group"><label>Penalty</label><input class="form-control" type="text" name="penalty" value="{{old("penalty")}}"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row text-center form-button form-details">
                                            <div class="col"><button class="btn btn-primary btn-lg" type="submit">Submit</button></div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
        $('#efind').on('click', function () {
            if ($("#ghpost_code").val()) {
                var code = $("#ghpost_code").val();
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"}});
                $.post("{{route('getOwnerPratesPost')}}", {ghpost: code}, function (data, status) {
                    console.log(data);
                    var response = $.parseJSON(data);
                    if (response.status == 80) {
                        var ownerid=response.owner.id;
                        var name = response.owner.name;
                        var occupation = response.owner.occupation;
                        var email = response.owner.email;
                        var phone = response.owner.phone;
                        var remarks = response.owner.remarks;
                        $("#ownerid").val(ownerid);
                        $("#eowner_picture").attr('src', response.owner.picture);
                        $("#eowner_occupation").text(occupation);
                        $("#eowner_name").text(name);
                        $("#eowner_email").text(email);
                        $("#eowner_phone").text(phone);
                        $("#eowner_remarks").text(remarks);
                        $("#eowner_details").show();
                        $("#eowner_prates_table").html("");
                        if (!$.isEmptyObject(response.prates)) {
                            for (let prate in response.prates) {
                                $("#eowner_prates_table").append("<tr>" +
                                        "<td>" + "<input type=radio name=prid value='" + response.prates[prate].prid + "' "+(prate==0 ? "checked=''":"")+">" + "</td>" +
                                        "<td>" + response.prates[prate].location + "</td>" +
                                        "<td>" + response.prates[prate].ghpost + "</td>" +
                                        "<td>" + response.prates[prate].size + "</td>" +
                                        "<td>" + response.prates[prate].district + "</td>" +
                                        "</tr>");
                            }

                            $("#eowner_prates").show();
                            $("#eowner_prates_error").hide();
                        } else {
                            $("#eowner_prates").hide();
                            $("#eowner_prates_error").show();
                        }

                        $("#eowner_error").text("");
                        $("#eowner_error").hide();
                    } else {

                        $("#eowner_details").hide();
                        $("#eowner_error").text("Property not found");
                        $("#eowner_error").show();
                        $("#eowner_prates").hide();
                        $("#eowner_prates_error").show();
                        $("#eowner_summons").hide();
                        $("#eowner_summons_error").show();
                    }

                });
            } else {
                $("#eowner_details").hide();
                $("#ownerid").val("Enter GH Post Code");
                $("#eowner_error").text("");
                $("#eowner_error").show();
            }
        });
    $('#ereset').on('click', function () {
        $("#eowner_details").hide();
        $("#ownerid").val("");
        $("#eowner_error").text("");
        $("#eowner_error").hide();
        $("#eid_number").val("");
        $("#eowner_prates_table").html("");
        $("#eowner_prates").hide();
        $("#eowner_prates_error").show();
    });
</script>
@endsection
