@extends('layouts.dash')
@section("css")
<link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/forms.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Summons</h5>
                </div>
                <div class="card-body">
                    <form>
                        @csrf
                        <div class="container">
                            <div class="form-row">
                                <div class="col">

                                    @if($errors->any())
                                    <div class="alert alert-danger" role="alert"><span><strong>{{$errors->first()}}</strong></span></div>
                                    @endif
                                    @if(Session::get("server_error"))
                                    <div class="alert alert-danger" role="alert"><span><strong>{{Session::get("server_error")}}</strong></span></div>
                                    @endif
                                    <div>

                                        <div class="form-row">
                                            <div class="col">
                                                <div>
                                                    <h5>Summons Details</h5>
                                                    <hr>
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group"><label>Type Of Building</label><input readonly="" class="form-control-plaintext" type="text" name="type" id="type" value="{{$summon->type_of_building}}"></div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group"><label>Stage Of Construction</label><input readonly="" class="form-control-plaintext" type="text" name="stage" value="{{$summon->stage_of_const}}"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group"><label>Penalty</label><input readonly="" class="form-control-plaintext" type="text" name="penalty" value="{{$summon->penalty}}"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row form-details">
                                            <div class="col">
                                                <h5>Property Details</h5>
                                                <hr>
                                                <div class="form-row">
                                                    <div class="col">
                                                        <a href="{{route("viewPrateDetails",['prid'=>$prate->prid])."#details"}}">View Property Details</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-row form-details">
                                            <div class="col">
                                                <h5>Related Permits</h5>
                                                <hr>
                                                @if(!empty($summon->pid))
                                                <div class="form-row">
                                                    @if($summon->type_of_permit=="building")
                                                    <div class="col">
                                                        <div class="form-group"><label>Type Of Permit</label><input readonly="" class="form-control-plaintext" type="text" value="Building Permit"></div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group"><label>Permit Details</label>
                                                            <div class="form-row">
                                                                <a href="{{route("viewBlpermitDetails",['blpid'=>$summon->pid])."#details"}}">View Permit Details</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @elseif($summon->type_of_permit=="signage")
                                                    <div class="col">
                                                        <div class="form-group"><label>Type Of Permit</label><input readonly="" class="form-control-plaintext" type="text" value="Signage & Adverts Permit"></div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group"><label>Permit Details</label>
                                                            <div class="form-row">
                                                                <a href="{{route("viewSpermitDetails",['spid'=>$summon->pid])."#details"}}">View Permit Details</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @elseif($summon->type_of_permit=="temp")
                                                    <div class="col">
                                                        <div class="form-group"><label>Type Of Permit</label><input readonly="" class="form-control-plaintext" type="text" value="Temporary Structure Permit"></div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group"><label>Permit Details</label>
                                                            <div class="form-row">
                                                                <a href="{{route("viewTspermitDetails",['tspid'=>$summon->pid])."#details"}}">View Permit Details</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @else
                                                    <div class="text-center">
                                                        <p>No related records found</p>
                                                    </div>
                                                    @endif
                                                </div>
                                                @else
                                                <div class="text-center">
                                                    <p>No related records found</p>
                                                </div>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
