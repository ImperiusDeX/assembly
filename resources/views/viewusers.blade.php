@extends('layouts.dash')
@section("css")
<link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/forms.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card" style="margin-bottom: 10px">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col-lg-12">


                                    <form action="{{route("viewUsers")}}" method="GET">

                                        <div class="form-group input-group">
                                            <input type="text" name="query" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" value="{{isset($query) ? $query:""}}">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="fas fa-search"></i>
                                                </button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if($errors->any())
            <div class="alert alert-danger" role="alert"><span><strong>{{$errors->first()}}</strong></span></div>
            @endif
            @if(Session::get("server_error"))
            <div class="alert alert-danger" role="alert"><span><strong>{{Session::get("server_error")}}</strong></span></div>
            @endif


            @if(Session::get("server_success"))
            <div class="alert alert-success" role="alert"><span><strong>{{Session::get("server_success")}}</strong></span></div>
            @endif

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-10">
                            <h3>Users</h3>
                            <h5>Manage</h5>
                        </div>
                        <div class="col-lg-2" style="padding-top:20px">
                            <a href="#addModal" data-toggle="modal"><button class="btn btn-primary" type="button"><i class="fas fa-fw fa-plus-square"></i> Add</button></a>
                        </div>
                    </div>

                    @if(!$users->isEmpty())
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Username</th>
                                    
                                    <th>District</th>
                                    <th>Actions</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user) 
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    
                                    <td>{{$user->district}}</td>
                                    <td><a href="{{route("viewUserDetails",['userid'=>$user->id])}}"><button class="btn btn-primary" type="button">View</button></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if(isset($query))
                    {{ $users->appends(["query"=>$query])->links() }}
                    @else
                    {{ $users->links() }}
                    @endif
                    @else
                    <div class="text-center">
                        <p>No records found</p>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5>Add User</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{route("addUser")}}" method="POST">
                    @csrf
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label>Surname</label><input class="form-control" type="text" name="surname" value="{{old("surname")}}"></div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label>First Name</label><input class="form-control" type="text" name="first_name" value="{{old("first_name")}}"></div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label>Email</label><input class="form-control" type="text" name="email" value="{{old("email")}}"></div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label>Phone Number</label><input class="form-control" type="text" name="phone" value="{{old("phone")}}"></div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                        <label>Roles</label>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>      
                                                        <th>Section</th>
                                                        <th>Due</th>
                                                        <th>Add</th>
                                                        <th>View</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="eowner_summons_table">
                                                    <tr>
                                                        <td>Property Rates</td>
                                                        <td><input type="checkbox" name="prate[]" value="1"></td>
                                                        <td><input type="checkbox" name="prate[]" value="2"></td>
                                                        <td><input type="checkbox" name="prate[]" value="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Building Permit</td>
                                                        <td><input type="checkbox" name="blpermit[]" value="1"></td>
                                                        <td><input type="checkbox" name="blpermit[]" value="2"></td>
                                                        <td><input type="checkbox" name="blpermit[]" value="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Business Permit</td>
                                                        <td><input type="checkbox" name="bupermit[]" value="1"></td>
                                                        <td><input type="checkbox" name="bupermit[]" value="2"></td>
                                                        <td><input type="checkbox" name="bupermit[]" value="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Signage & Adverts</td>
                                                        <td><input type="checkbox" name="spermit[]" value="1"></td>
                                                        <td><input type="checkbox" name="spermit[]" value="2"></td>
                                                        <td><input type="checkbox" name="spermit[]" value="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Temporary Structure</td>
                                                        <td><input type="checkbox" name="tspermit[]" value="1"></td>
                                                        <td><input type="checkbox" name="tspermit[]" value="2"></td>
                                                        <td><input type="checkbox" name="tspermit[]" value="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Summons</td>
                                                        <td></td>
                                                        <td><input type="checkbox" name="summon[]" value="2"></td>
                                                        <td><input type="checkbox" name="summon[]" value="3"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label>Password</label><input class="form-control" type="password" name="password" value=""></div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label>Confirm Password</label><input class="form-control" type="password" name="password_confirmation" value=""></div>
                                    </div>
                                </div>
                                <div class="form-row text-center form-button">
                                    <div class="col"><button class="btn btn-primary btn-lg" type="submit">Submit</button></div>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
@endsection
