@extends('layouts.dash')
@section("css")
<link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
<link href="{{ asset('css/forms.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body text-center">
                     Property Rate record was successfully created
                      @if(auth()->user()->role=="admin")
                     <div class="row" style="margin-top: 30px">                       
                         <div class="col-lg-3 text-right">
                             <a href="{{route("addBlpermitForm",["prid"=>$prid])}}"><button class="btn btn-primary btn-lg" type="submit">Add Building Permit</button></a>
                         </div>
                         <div class="col-lg-3 text-left">
                             <a href="{{route("addBupermitForm",["prid"=>$prid])}}"><button class="btn btn-primary btn-lg" type="submit">Add Business Permit</button></a>
                         </div>
                         <div class="col-lg-3 text-right">
                             <a href="{{route("addBlpermitForm",["prid"=>$prid])}}"><button class="btn btn-primary btn-lg" type="submit">Print Invoice</button></a>
                         </div>
                         <div class="col-lg-3 text-center">
                             <a href="{{route("addBupermitForm",["prid"=>$prid])}}"><button class="btn btn-primary btn-lg" type="submit">Print Receipt</button></a>
                         </div>                        
                     </div> 
                      @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
